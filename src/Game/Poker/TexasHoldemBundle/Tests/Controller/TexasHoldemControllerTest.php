<?php

namespace Game\Poker\TexasHoldemBundle\Tests;

use Game\Poker\TexasHoldemBundle\Controller;

class TexasHoldemControllerTest extends \PHPUnit_Framework_TestCase {
    
    public function testCheckBankroll(){
        $check = new TexasHoldemPokerController;
        $result = $check->checkBankroll(50);
        
        $this->assertEquals(40, $result);
    }
}