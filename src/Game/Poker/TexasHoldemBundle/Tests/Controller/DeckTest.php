<?php

namespace Game\Poker\TexasHoldemBundle\Tests;

use Game\Poker\TexasHoldemBundle\Entity\Deck;
use Game\Poker\TexasHoldemBundle\Entity\Card;
use Game\Poker\TexasHoldemBundle\Entity\DeckCard;


class DeckTest extends \PHPUnit_Framework_TestCase {
    
    public function testDelete(){
    	$card = new Card();
    	$this->card->setRank('Coeur')->setSuit('2');
        $deck = new Deck($this->card);
        
        $this->assertEquals('Coeur', $this->deck->getDeckCard());
    }
}