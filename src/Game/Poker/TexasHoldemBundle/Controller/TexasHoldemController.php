<?php

namespace Game\Poker\TexasHoldemBundle\Controller ;

use Symfony\Bundle\FrameworkBundle\Controller\Controller ;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route ;
use Symfony\Component\HttpFoundation\Request ;
use \Symfony\Component\HttpFoundation\JsonResponse ;
use Game\Poker\CoreBundle\Controller\CorePokerController ;


class TexasHoldemController extends CorePokerController
{

    protected $variant = 'texas-holdem' ;

    /**
     * @Route("/poker/texas-holdem/solo", name="poker_texas-holdem_solo")
     */
    public function soloTexasHoldemAction( Request $request )
    {
        $this -> mode = 'solo' ;

        $form = $request -> query -> all() ;

        var_dump( $form ) ;
        die() ;

        return $this -> render( 'GamePokerTexasHoldemBundle:Default:index.html.twig' ) ;
    }


    /**
     * @Route("/poker/texas-holdem/multi/game/{gameId}/table/{tableId}", name="poker_texas-holdem_multi")
     */
    public function multiTexasHoldemAction( $gameId = 0 , $tableId = 0 )
    {
        $this -> mode = 'multi' ;

        $this -> initializeGame( $gameId , $tableId ) ;

        if( 0 == $gameId )

            return $this -> redirect(
                                        $this -> generateUrl(
                                                                'poker_texas-holdem_multi' , array(
                                                                                                    'gameId'  => $this -> game  -> getId() ,

                                                                                                    'tableId' => $this -> table -> getId()
                                                                                                  )
                                                            )
                                    )
            ;


        else

            return $this -> render( 'GamePokerTexasHoldemBundle:Table:table.html.twig' , array(     'Game'        => $this -> game ,

                                                                                                    'Table'       => $this -> table ,

                                                                                                    'Round'       => $this -> round ,

                                                                                                    'Deck'        => $this -> deck ,

                                                                                                    'Player'      => $this -> player ,

                                                                                                    'GameSession' => $this -> gameSession ,

                                                                                                    'messages'    => $this -> sessionMessages )  ) ;

    }


    private function newRoundInit()
    {

        $this -> gameSession -> sethasPlayed( false ) ;

        $this -> round -> setIsPlayTime( false ) ;

        $this -> round -> setBetsAreEqual( false ) ;

        $this -> round -> setIsReady( true ) ;

        $this -> round -> setIsWaiting( true ) ;

        $this -> round -> setCurrentBet( 0 ) ;

        $this -> round -> setLastBet( 0 ) ;

        $this -> table -> setPot() ;

    }


    /**
     * @Route("/poker/texas-holdem/multi/game/{gameId}/table/{tableId}/getCards", name="poker_texas-holdem_multi_getCards")
     */
    public function getCardsAction( $gameId , $tableId )
    {

        $this -> refreshRound( $gameId , $tableId ) ;

        if( ! $this -> gameSession -> hasHand() AND  $this -> gameSession -> getIsInPlay() )
        {

            $this -> dealer -> dealCardsTo( $this -> gameSession , $this -> deck ) ;

            $this -> deck -> deleteCards( $this -> em ) ;

            if( $this -> table -> everyBodyHasCards() )

                $this -> round -> setIsComplete( true) ;

        }

        $hand = $this -> gameSession -> getHand() -> getTransformedHand() ;

        $this -> round -> setAllowedPlayer( $this -> table -> getNextValidPlayer() ) ;

        $this -> em -> flush() ;

        return  new JsonResponse( $hand ) ;
    }


    /**
     * @Route("/poker/texas-holdem/game/{gameId}/table/{tableId}/call/{amount}", name="poker_texas-holdem_call")
     * @Route("/poker/texas-holdem/game/{gameId}/table/{tableId}/bet-amount/{amount}", name="poker_texas-holdem_bet_amount")
     */
    public function betAction( $gameId , $tableId , $amount = null )
    {

        $this -> refreshRound( $gameId , $tableId ) ;

        $this -> round -> setBetsAreEqual( false ) ;


        if( null == $amount )

            $amount = $this -> round -> getCurrentBet() - $this -> gameSession -> getBet() ;


        $this -> player -> betChipsOn( $this -> table , $amount ) ;

        $this -> gameSession -> setPreviousAction( 'bet/' . $amount ) ;

        $this -> gameSession -> setHasPlayed( true ) ;

        $this -> table -> setCurrentPlayer( $this -> table -> getNextValidPlayer( $this -> player ) ) ;


        if( $this -> table -> hasEqualBets() )

            $this -> round -> setBetsAreEqual( true ) ;


        if( $this -> round -> getBetsAreEqual() AND $this -> table -> everyBodyHasPlayed() )

            $this -> newRoundInit() ;


        $this -> round -> setAllowedPlayer( $this -> table -> getNextValidPlayer() ) ;

        $this -> em -> flush() ;

        return  new JsonResponse( [ 'status' => 'done' , 'messages' =>  $this -> table -> getMessages() , 'nextAction' => array() ] ) ;
    }


    /**
     * @Route("/poker/texas-holdem/game/{gameId}/table/{tableId}/check", name="poker_texas-holdem_check")
     */
    public function checkAction( $gameId , $tableId )
    {

        $this -> refreshRound( $gameId , $tableId ) ;

        $this -> gameSession -> setPreviousAction( 'checked' ) ;

        $this -> gameSession -> setHasPlayed( true ) ;

        $this -> table -> setCurrentPlayer( $this -> table -> getNextValidPlayer( $this -> player ) ) ;


        if( $this -> table -> hasEqualBets() )

            $this -> round -> setBetsAreEqual( true ) ;


        if( $this -> round -> getBetsAreEqual() AND $this -> table -> everyBodyHasPlayed() )

            $this -> newRoundInit() ;


        $this -> round -> setAllowedPlayer( $this -> table -> getNextValidPlayer() ) ;

        $this -> em -> flush() ;

        return  new JsonResponse( [ 'status' => 'done' , 'messages' =>  $this -> table -> getMessages() , 'nextAction' => array() ] ) ;
    }


    /**
     * @Route("/poker/texas-holdem/game/{gameId}/table/{tableId}/get-flop", name="poker_texas-holdem_get_flop")
     * @Route("/poker/texas-holdem/game/{gameId}/table/{tableId}/get-turn", name="poker_texas-holdem_get_turn")
     * @Route("/poker/texas-holdem/game/{gameId}/table/{tableId}/get-river", name="poker_texas-holdem_get_river")
     */
    public function getRoundAction( $gameId = null , $tableId = null  )
    {

        $this -> refreshRound( $gameId , $tableId ) ;

        $this -> round -> setIsReady( false ) ;

        $this -> round -> setIsWaiting( false ) ;

        $this -> round -> setIsPlayTime( true ) ;

        $this -> round -> setIsComplete( true ) ;

        $roundName = $this -> round ->getName() ;

        switch( $roundName )
        {
            case 'FLOP' :
                $cardsNumber = 3 ;
                break ;

            case 'TURN' :
            case 'RIVER' :

                $cardsNumber = 1 ;
                break ;
        }

        $cards = $this -> dealer -> dealBoardCards( $this -> deck , $cardsNumber ) ;

        $this -> table -> setBoardCards( $cards ) ;

        $this -> deck -> deleteCards( $this -> em ) ;

        $this -> table -> addMessage( $roundName . ' distribué !' ) ;

        $this -> round -> setAllowedPlayer( $this -> table -> getNextValidPlayer() ) ;

        $this -> em -> flush() ;

        return new JsonResponse( [ 'status' => 'done' , 'messages' =>  $this -> table -> getMessages() , 'nextAction' => array() ] ) ;



    }


    /**
     * @Route("/poker/texas-holdem/game/{gameId}/table/{tableId}/showdown", name="poker_texas-holdem_showdown")
     */
    public function showDownAction( $gameId , $tableId )
    {

        $this -> refreshRound( $gameId , $tableId ) ;



        return  new JsonResponse( [ 'status' => 'done' , 'messages' =>  $this -> table -> getMessages() , 'nextAction' => array() ] ) ;
    }



}
