<?php
/**
 * Created by PhpStorm.
 * User: jo
 * Date: 06/02/17
 * Time: 11:49
 */

namespace Game\Poker\CoreBundle\Resources ;

const NOT_ENOUGH_MONEY = 'Désolé, vous n\'avez plus assez d\'argent sur votre compte pour jouer à cette table aujourd\'hui, revenez demain ! :)' ;