<?php

namespace Game\Poker\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Game\Poker\CoreBundle\Entity\Card;
use Game\Poker\CoreBundle\Entity\DeckCard;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Deck
 *
 * @ORM\Table(name="deck")
 * @ORM\Entity(repositoryClass="Game\Poker\CoreBundle\Repository\DeckRepository")
 */
class Deck {


    private $toBeDeletedDeckCard = array();

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Game\Poker\CoreBundle\Entity\DeckCard", mappedBy="deck", cascade={"persist"})
     */
    private $deckCards;

    public function __construct() {
        $this->deckCards = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    public function setCards( $cards )
    {
        //Creer manuellement chaque deckCard de liaison
        shuffle($cards);

        foreach ($cards as $card)
        {
            //Donne la carte à DeckCard
            $deckCard = new DeckCard($card);
            //DeckCard à Deck
            $this->addDeckCard($deckCard);
        }
    }

    public function getDeckCards() {
        return $this->deckCards;
    }

    /**
     * Add card
     *
     * @param Card $card
     *
     * @return Deck
     */
    public function addDeckCard(DeckCard $deckCard) {
        $deckCard->setDeck($this);
        $this->deckCards[] = $deckCard;

        return $this;
    }

    /**
     * Remove card
     *
     * @param \Game\Poker\CoreBundle\Entity\Card $card
     */
    public function removeDeckCard(DeckCard $deckCard)
    {

        $this -> deckCards -> removeElement( $deckCard ) ;

        $deckCard -> getCard() -> removeDeck( $deckCard ) ;

        $this -> toBeDeletedDeckCard[] = $deckCard ;
        
    }

    public function extractCard( $i = 2 , $boardCards = false )
    {
        $cards = array();

        for( $j = 0 ; $j < $i ; $j++)
        {
            $rand = mt_rand( 0 , count( $this -> deckCards ) -1 ) ;

            $deckCard = $this -> deckCards[ $rand ] ;

            if( $boardCards )
            {
                $boardCard = new BoardCard() ;
                $boardCard -> setCard( $deckCard -> getCard() ) ;
                $cards[] = $boardCard ;
            }
            else

                $cards[] = $deckCard -> getCard() ;

            $this -> removeDeckCard( $deckCard ) ;
        }

        return $cards;
    }

    public function deleteCards( $em )
    {
        foreach( $this -> toBeDeletedDeckCard as $deckCard )

            $em -> remove( $deckCard ) ;
    }
}
