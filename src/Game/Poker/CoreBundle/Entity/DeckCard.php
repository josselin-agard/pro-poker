<?php

namespace Game\Poker\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Game\Poker\CoreBundle\Entity\Deck ;
use Game\Poker\CoreBundle\Entity\Card ;

/**
 * DeckCard
 *
 * @ORM\Table(name="deck_card")
 * @ORM\Entity(repositoryClass="Game\Poker\CoreBundle\Repository\DeckCardRepository")
 */
class DeckCard {

    public function __construct($card) {
        $this->setCard($card);
        $this->setFaceUp(false);
    }


    /**
     * @var int
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Game\Poker\CoreBundle\Entity\Deck", inversedBy="cards",cascade={"persist"})
     * @ORM\JoinColumn(name="deck_id", referencedColumnName="id")
     */
    private $deck;

    /**
     * @var int
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Game\Poker\CoreBundle\Entity\Card", inversedBy="decks",cascade={"persist"})
     * @ORM\JoinColumn(name="card_id", referencedColumnName="id")
     */
    private $card;

    /**
     * @var boolean
     *
     * @ORM\Column(name="faceUp", type="boolean")
     */
    private $faceUp;

    /**
     * @var boolean
     *
     * @ORM\Column(name="dealt", type="boolean", nullable=true)
     */
    private $dealt;
    
    /**
     * Set faceUp
     *
     * @param boolean $faceUp
     *
     * @return DeckCard
     */
    public function setFaceUp($faceUp) {
        $this->faceUp = $faceUp;

        return $this;
    }

    /**
     * Get faceUp
     *
     * @return boolean
     */
    public function getFaceUp() {
        return $this->faceUp;
    }

    /**
     * Set deck
     *
     * @param  $deck
     *
     * @return DeckCard
     */
    public function setDeck(Deck $deck = null) {
        $this->deck = $deck;

        return $this;
    }

    /**
     * Get deck
     *
     * @return 
     */
    public function getDeck() {
        return $this->deck;
    }

    /**
     * Set card
     *
     * @param  $card
     *
     * @return DeckCard
     */
    public function setCard(Card $card = null) {
        if($card !== null) $card->addDeck($this);
        $this->card = $card;

        return $this;
    }

    /**
     * Get card
     *
     * @return 
     */
    public function getCard() {
        return $this->card;
    }


    /**
     * Set dealt
     *
     * @param boolean $dealt
     *
     * @return DeckCard
     */
    public function setDealt($dealt)
    {
        $this->dealt = $dealt;
    
        return $this;
    }

    /**
     * Get dealt
     *
     * @return boolean
     */
    public function getDealt()
    {
        return $this->dealt;
    }
}
