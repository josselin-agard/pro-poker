<?php

namespace Game\Poker\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BoardCard
 *
 * @ORM\Table(name="board_card")
 * @ORM\Entity(repositoryClass="Game\Poker\CoreBundle\Repository\BoardCardRepository")
 */
class BoardCard
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Game\Poker\CoreBundle\Entity\Card",cascade={"persist"})
    
     */
    private $card;

    /**
     * @ORM\ManyToOne(targetEntity="Game\Poker\CoreBundle\Entity\PokerTable", cascade={"persist"})
     */
    private $table ;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this -> id ;
    }

    /**
     * Set card
     *
     * @param \Game\Poker\CoreBundle\Entity\Card $card
     *
     * @return BoardCard
     */
    public function setCard( \Game\Poker\CoreBundle\Entity\Card $card = null )
    {
        $this -> card = $card ;

        return $this ;
    }

    /**
     * Get card
     *
     * @return \Game\Poker\CoreBundle\Entity\Card
     */
    public function getCard()
    {
        return $this -> card ;
    }

    /**
     * Set table
     *
     * @param \Game\Poker\CoreBundle\Entity\PokerTable $table
     *
     * @return BoardCard
     */
    public function setTable( \Game\Poker\CoreBundle\Entity\PokerTable $table = null )
    {
        $this -> table = $table ;

        return $this ;
    }

    /**
     * Get table
     *
     * @return \Game\Poker\CoreBundle\Entity\PokerTable
     */
    public function getTable()
    {
        return $this -> table ;
    }
}
