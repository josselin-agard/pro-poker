<?php

namespace Game\Poker\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Card
 *
 * @ORM\Table(name="card")
 * @ORM\Entity(repositoryClass="Game\Poker\CoreBundle\Repository\CardRepository")
 */
class Card {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="suit", type="string", length=255)
     */
    private $suit;
    
    /**
     * @var string
     *
     * @ORM\Column(name="rank", type="string", length=255)
     */
    private $rank;
    
    /**
     * @ORM\OneToMany(targetEntity="Game\Poker\CoreBundle\Entity\DeckCard", mappedBy="card", cascade={"persist"})
     */
    private $decks;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set rank
     *
     * @param integer $rank
     *
     * @return Card
     */
    public function setRank($rank) {
        $this->rank = $rank;

        return $this;
    }

    /**
     * Get rank
     *
     * @return integer
     */
    public function getRank() {
        return $this->rank;
    }

    /**
     * Set suit
     *
     * @param string $suit
     *
     * @return Card
     */
    public function setSuit($suit) {
        $this->suit = $suit;

        return $this;
    }

    /**
     * Get suit
     *
     * @return string
     */
    public function getSuit() {
        return $this->suit;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->decks = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add deck
     *
     * @param \Game\Poker\CoreBundle\Entity\DeckCard $deck
     *
     * @return Card
     */
    public function addDeck(\Game\Poker\CoreBundle\Entity\DeckCard $deckCard)
    {
        $this->decks[] = $deckCard;
    
        return $this;
    }

    /**
     * Remove deck
     *
     * @param \Game\Poker\CoreBundle\Entity\DeckCard $deck
     */
    public function removeDeck(\Game\Poker\CoreBundle\Entity\DeckCard $deckCard)
    {
        $this->decks->removeElement($deckCard);
    }

    /**
     * Get decks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDecks()
    {
        return $this->decks;
    }
}
