<?php

namespace Game\Poker\CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Game\Poker\CoreBundle\Entity\Game;

/**
 * Round
 *
 * @ORM\Table(name="round")
 * @ORM\Entity(repositoryClass="Game\Poker\CoreBundle\Repository\RoundRepository")
 */
class PokerRound {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id ;

    /**
     * @var string
     *
     * @ORM\Column(name="currentSubRound", type="string", length=255, nullable=true)
     */
    private $currentSubRound ;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name ;

    /**
     * @var string
     *
     * @ORM\Column(name="rank", type="integer", nullable=true)
     */
    private $rank ;

    /**
     * @var string
     *
     * @ORM\Column(name="lastrank", type="integer", nullable=true)
     */
    private $lastRank ;

    /**
     * @var string
     *
     * @ORM\Column(name="nextrank", type="integer", nullable=true)
     */
    private $nextRank ;

    /**
     * @var string
     *
     * @ORM\Column(name="nextRound", type="string", length=255, nullable=true)
     */
    private $nextRound ;

    /**
     * @var string
     *
     * @ORM\Column(name="lastRound", type="string", length=255, nullable=true)
     */
    private $lastRound ;

    /**
     * @var string
     *
     * @ORM\Column(name="lastAction", type="string", length=255, nullable=true)
     */
    private $lastAction ;

    /**
     * @var string
     *
     * @ORM\Column(name="currentAction", type="string", length=255, nullable=true)
     */
    private $currentAction ;

    /**
     * @var string
     *
     * @ORM\Column(name="nextAction", type="string", length=255, nullable=true)
     */
    private $nextAction ;

    /**
     * @ORM\ManyToOne(targetEntity="Game\Poker\CoreBundle\Entity\PokerUser")
     */
    private $lastPlayer ;

    /**
     * @ORM\ManyToOne(targetEntity="Game\Poker\CoreBundle\Entity\PokerUser")
     */
    private $currentPlayer ;

    /**
     * @ORM\ManyToOne(targetEntity="Game\Poker\CoreBundle\Entity\PokerUser")
     */
    private $allowedPlayer ;

    /**
     * @ORM\ManyToOne(targetEntity="Game\Poker\CoreBundle\Entity\PokerUser")
     */
    private $nextPlayer ;

    /**
     * @var string
     *
     * @ORM\Column(name="lastBet", type="float", nullable=true)
     */
    private $lastBet ;

    /**
     * @var string
     *
     * @ORM\Column(name="currentBet", type="float", nullable=true)
     */
    private $currentBet ;

    /**
     * @ORM\ManyToOne(targetEntity="Game\Poker\CoreBundle\Entity\PokerUser", cascade={"persist"})
     */
    private $bigBlindPlayer ;

    /**
     * @ORM\ManyToOne(targetEntity="Game\Poker\CoreBundle\Entity\PokerUser", cascade={"persist"})
     */
    private $smallBlindPlayer ;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isComplete", type="boolean", nullable=true)
     */
    private $isComplete;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isReady", type="boolean", nullable=true)
     */
    private $isReady;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isWaiting", type="boolean", nullable=true)
     */
    private $isWaiting;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isGameStarted", type="boolean", nullable=true)
     */
    private $isGameStarted;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isPlayTime", type="boolean", nullable=true)
     */
    private $isPlayTime;

    /**
     * @var boolean
     *
     * @ORM\Column(name="betsAreEqual", type="boolean", nullable=true)
     */
    private $betsAreEqual;



    private $rounds = [ 'INIT' , 'FLOP' , 'TURN' , 'RIVER', 'SHOWDOWN' ] ;
    private $result;

    public function __construct( PokerUser $currentPlayer = null , PokerUser $nextPlayer = null )
    {
        $this -> name = 'INIT' ;
        $this -> rank = 0 ;

        $this -> currentAction = 'initializeGame' ;
        $this -> nextAction = 'getCards' ;

        $this -> currentPlayer = $currentPlayer ;
        $this -> nextPlayer = $nextPlayer ;

        $this -> isReady = false ;
        $this -> isComplete = false ;
        $this -> isWaiting = false ;
        $this -> isGameStarted = false ;
        $this -> isPlayTime = false ;
        $this -> betsAreEqual = false ;

        $this -> nextRound = 'FLOP' ;
        $this -> nextRank = 1 ;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this -> id ;
    }


    public function getResult()
    {
        if ( empty( $this -> result ) )

            return $this -> currentSubRound -> getResult() ;

        else
            return $this -> result ;
    }


    function getCurrentSubRound()
    {
        return $this -> currentSubRound ;
    }


    function getName()
    {
        return $this -> name ;
    }


    function getNextRound()
    {
        return $this -> nextRound ;
    }


    function getLastRound()
    {
        return $this -> lastRound ;
    }


    function getLastAction()
    {
        return $this -> lastAction ;
    }



    function getCurrentAction()
    {
        return $this -> currentAction ;
    }


    function getNextAction()
    {
        return $this -> nextAction ;
    }


    function getLastPlayer()
    {
        return $this -> lastPlayer ;
    }


    function getCurrentPlayer()
    {
        return $this -> currentPlayer ;
    }


    function getNextPlayer()
    {
        return $this -> nextPlayer ;
    }


    function getLastBet()
    {
        return $this -> lastBet ;
    }


    function getCurrentBet()
    {
        return $this -> currentBet ;
    }


    function getIsComplete()
    {
        return $this -> isComplete ;
    }


    function getIsReady()
    {
        return $this -> isReady ;
    }


    function setCurrentSubRound( $currentSubRound )
    {
        $this -> currentSubRound = $currentSubRound ;
    }


    function setName( $name )
    {
        $this -> name = $name ;
    }


    function setNextRound( $nextRound = null )
    {
        if( $nextRound == null AND isset( $this -> rounds [ ( $index = array_search( $this -> name , $this -> rounds ) + 1 ) ] ) )
        {
            $nextRound = $this -> rounds [ $index ] ;

            $this -> nextRank = $index ;
        }

        $this -> nextRound = $nextRound ;
    }


    function setLastRound( $lastRound )
    {
        $this -> lastRound = $lastRound ;
    }


    function setLastAction( $lastAction )
    {
        $this -> lastAction = $lastAction ;
    }


    function setCurrentAction( $currentAction )
    {
        $this -> currentAction = $currentAction ;
    }


    function setNextAction( $nextAction = null )
    {
        $this -> nextAction = $nextAction ;
    }


    function setLastPlayer( PokerUser $lastPlayer )
    {
        $this -> lastPlayer = $lastPlayer ;
    }


    function setCurrentPlayer( PokerUser $currentPlayer )
    {
        $this -> currentPlayer = $currentPlayer ;
    }


    function setNextPlayer( PokerUser $nextPlayer )
    {
        $this -> nextPlayer = $nextPlayer ;
    }


    function setLastBet( $lastBet )
    {
        $this -> lastBet = $lastBet ;
    }


    function setCurrentBet( $currentBet )
    {
        $this -> lastBet = $this -> currentBet ;

        $this -> currentBet = $currentBet ;
    }


    function setIsComplete( $isComplete )
    {
        $this -> isComplete = $isComplete ;
    }


    function setIsReady ( $isReady )
    {

        $this -> isReady = $isReady ;

        if ( $this -> isReady )
        {
            $this -> lastRound = $this -> getName() ;

            $this -> lastRank =  $this -> rank ;

            $this -> name = $this -> getNextRound() ;

            $this -> rank = $this -> getNextRank() ;

            $this -> setNextRound() ;

            $this -> setIsWaiting( true ) ;

            $this -> setIsComplete( false ) ;
        }
    }


    /**
     * Set rank
     *
     * @param integer $rank
     *
     * @return PokerRound
     */
    public function setRank( $rank )
    {
        $this -> rank = $rank ;

        return $this ;
    }


    /**
     * Get rank
     *
     * @return integer
     */
    public function getRank()
    {
        return $this -> rank ;
    }


    /**
     * Set lastRank
     *
     * @param integer $lastRank
     *
     * @return PokerRound
     */
    public function setLastRank( $lastRank )
    {
        $this -> lastRank = $lastRank ;

        return $this ;
    }


    /**
     * Get lastRank
     *
     * @return integer
     */
    public function getLastRank()
    {
        return $this -> lastRank ;
    }


    /**
     * Set nextRank
     *
     * @param integer $nextRank
     *
     * @return PokerRound
     */
    public function setNextRank( $nextRank )
    {
        $this -> nextRank = $nextRank ;

        return $this ;
    }


    /**
     * Get nextRank
     *
     * @return integer
     */
    public function getNextRank()
    {
        return $this -> nextRank ;
    }


    /**
     * Set bigBlindPlayer
     *
     * @param PokerUser $bigBlindPlayer
     *
     * @return PokerRound
     */
    public function setBigBlindPlayer( PokerTable $table , PokerUser $bigBlindPlayer = null , $bigBlindAmount )
    {
        $bigBlindPlayer -> betChipsOn( $table , $bigBlindAmount ) ;

        $this -> bigBlindPlayer = $bigBlindPlayer ;

        return $this ;
    }


    /**
     * Get bigBlindPlayer
     *
     * @return \Game\Poker\CoreBundle\Entity\PokerUser
     */
    public function getBigBlindPlayer()
    {
        return $this -> bigBlindPlayer ;
    }


    /**
     * Set smallBlindPlayer
     *
     * @param PokerUser $smallBlindPlayer
     *
     * @return PokerRound
     */
    public function setSmallBlindPlayer( PokerTable $table , PokerUser $smallBlindPlayer = null , $smallBlindAmount )
    {
        $smallBlindPlayer -> betChipsOn( $table, $smallBlindAmount ) ;

        $this -> smallBlindPlayer = $smallBlindPlayer;

        return $this ;
    }


    /**
     * Get smallBlindPlayer
     *
     * @return PokerUser
     */
    public function getSmallBlindPlayer()
    {
        return $this -> smallBlindPlayer ;
    }


    /**
     * Set waiting
     *
     * @param boolean $waiting
     *
     * @return PokerRound
     */
    public function setIsWaiting( $waiting )
    {
        $this -> isWaiting = $waiting ;

        return $this ;
    }


    /**
     * Get waiting
     *
     * @return boolean
     */
    public function getIsWaiting()
    {
        return $this -> isWaiting ;
    }


    /**
     * Set isGameStarted
     *
     * @param boolean $isGameStarted
     *
     * @return PokerRound
     */
    public function setIsGameStarted( $isGameStarted )
    {
        $this -> isGameStarted = $isGameStarted ;

        return $this ;
    }


    /**
     * Get isGameStarted
     *
     * @return boolean
     */
    public function getIsGameStarted()
    {
        return $this -> isGameStarted ;
    }


    /**
     * Set isPlayTime
     *
     * @param boolean $isPlayTime
     *
     * @return PokerRound
     */
    public function setIsPlayTime( $isPlayTime )
    {
        $this -> isPlayTime = $isPlayTime ;

        return $this ;
    }

    /**
     * Get isPlayTime
     *
     * @return boolean
     */
    public function getIsPlayTime()
    {
        return $this -> isPlayTime ;
    }


    /**
     * Set betsAreEqual
     *
     * @param boolean $betsAreEqual
     *
     * @return PokerRound
     */
    public function setBetsAreEqual( $betsAreEqual )
    {
        $this -> betsAreEqual = $betsAreEqual ;

        return $this ;
    }

    /**
     * Get betsAreEqual
     *
     * @return boolean
     */
    public function getBetsAreEqual()
    {
        return $this -> betsAreEqual ;
    }

    /**
     * Set allowedPlayer
     *
     * @param \Game\Poker\CoreBundle\Entity\PokerUser $allowedPlayer
     *
     * @return PokerRound
     */
    public function setAllowedPlayer( \Game\Poker\CoreBundle\Entity\PokerUser $allowedPlayer = null )
    {
        $this -> allowedPlayer = $allowedPlayer ;

        return $this ;
    }

    /**
     * Get allowedPlayer
     *
     * @return \Game\Poker\CoreBundle\Entity\PokerUser
     */
    public function getAllowedPlayer()
    {
        return $this -> allowedPlayer ;
    }
}
