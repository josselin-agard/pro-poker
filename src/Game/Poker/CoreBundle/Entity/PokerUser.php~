<?php

namespace Game\Poker\CoreBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

use TWS\UserBundle\Entity\User;
use Game\Poker\CoreBundle\Traits\PokerPlayer;

/**
 * PokerUser
 *
 * @ORM\Table(name="poker_user")
 * @ORM\Entity(repositoryClass="Game\Poker\CoreBundle\Repository\PokerUserRepository")
 */
class PokerUser extends User
{

    use PokerPlayer ;

    private $rewards ;
    private $statistics ;


    public function __construct()
    {
        parent::__construct() ;

        $this -> tables = new ArrayCollection() ;
        $this -> bankroll = 1000 ;

    }
    

    function getRewards()
    {
        return $this -> rewards ;
    }

    function getStatistics()
    {
        return $this -> statistics ;
    }


    function setRewards($rewards)
    {
        $this->rewards = $rewards ;
    }

    function setStatistics( $statistic )
    {
        $this -> statistic = $statistic ;
    }


    /**
     * Set bankroll
     *
     * @param integer $bankroll
     *
     * @return PokerUser
     */
    public function setBankroll($bankroll)
    {
        $this->bankroll = $bankroll;

        return $this;
    }

    /**
     * Get bankroll
     *
     * @return integer
     */
    public function getBankroll()
    {
        return $this->bankroll;
    }

    /**
     * Add gameSession
     *
     * @param \Game\Poker\CoreBundle\Entity\GameSession $gameSession
     *
     * @return PokerUser
     */
    public function addGameSession(\Game\Poker\CoreBundle\Entity\GameSession $gameSession)
    {
        $this->gameSessions[] = $gameSession;

        return $this;
    }

    /**
     * Remove gameSession
     *
     * @param \Game\Poker\CoreBundle\Entity\GameSession $gameSession
     */
    public function removeGameSession(\Game\Poker\CoreBundle\Entity\GameSession $gameSession)
    {
        $this->gameSessions->removeElement($gameSession);
    }

    /**
     * Get gameSessions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGameSessions()
    {
        return $this->gameSessions;
    }

    /**
     * Add table
     *
     * @param \Game\Poker\CoreBundle\Entity\PokerTable $table
     *
     * @return PokerUser
     */
    public function addTable(\Game\Poker\CoreBundle\Entity\PokerTable $table)
    {
        $this->tables[] = $table;

        return $this;
    }

    /**
     * Remove table
     *
     * @param \Game\Poker\CoreBundle\Entity\PokerTable $table
     */
    public function removeTable(\Game\Poker\CoreBundle\Entity\PokerTable $table)
    {
        $this->tables->removeElement($table);
    }

    /**
     * Get tables
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTables()
    {
        return $this->tables;
    }
}
