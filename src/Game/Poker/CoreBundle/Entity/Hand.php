<?php

namespace Game\Poker\CoreBundle\Entity ;

use Doctrine\ORM\Mapping as ORM ;
use Doctrine\Common\Collections\ArrayCollection ;

use Game\Poker\CoreBundle\Entity\GameSession ;
use Game\Poker\CoreBundle\Entity\Card ;

/**
 * Hand
 *
 * @ORM\Table(name="hand")
 * @ORM\Entity(repositoryClass="Game\Poker\CoreBundle\Repository\HandRepository")
 */
class Hand
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\OneToOne(targetEntity="Game\Poker\CoreBundle\Entity\PokerUser", cascade={"persist"})
     */
    private $gameSession ;


    /**
     * @ORM\ManyToMany(targetEntity="Game\Poker\CoreBundle\Entity\Card", cascade={"persist"})
     */
    private $cards ;



    public function __construct()
    {
        $this -> cards = new ArrayCollection() ;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this -> id ;
    }



    /**
     * Add card
     *
     * @param \Game\Poker\CoreBundle\Entity\Card $card
     *
     * @return Hand
     */
    public function addCard( Card $card )
    {
        $this -> cards[] = $card ;

        return $this ;
    }

    /**
     * Remove card
     *
     * @param \Game\Poker\CoreBundle\Entity\Card $card
     */
    public function removeCard( Card $card )
    {
        $this -> cards -> removeElement( $card ) ;
    }

    /**
     * Get cards
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCards()
    {
        return $this -> cards ;
    }


    /**
     * Set gameSession
     *
     * @param \Game\Poker\CoreBundle\Entity\PokerUser $gameSession
     *
     * @return Hand
     */
    public function setGameSession(\Game\Poker\CoreBundle\Entity\PokerUser $gameSession = null)
    {
        $this->gameSession = $gameSession;

        return $this;
    }

    /**
     * Get gameSession
     *
     * @return \Game\Poker\CoreBundle\Entity\PokerUser
     */
    public function getGameSession()
    {
        return $this->gameSession;
    }


    public function getTransformedHand()
    {
        $cards = array() ;

        foreach ( $this -> getCards() as $card )

            $cards[] = [ $card -> getRank() , $card -> getSuit() ] ;


        return $cards ;
    }

    public function isEmpty()
    {
        return ( 0 == count ( $this -> getCards() ) ) ? true : false ;
    }
}
