<?php

namespace Game\Poker\CoreBundle\Entity ;

use Doctrine\ORM\Mapping as ORM ;
use Doctrine\Common\Collections\ArrayCollection ;

use Game\Poker\CoreBundle\Entity\Game ;
use Game\Poker\CoreBundle\Entity\Card ;
use Game\Poker\CoreBundle\Entity\PokerUser ;
use Game\Poker\CoreBundle\Models\Dealer ;
use Game\Poker\CoreBundle\Entity\PokerRound as Round ;

/**
 * PokerTable
 *
 * @ORM\Table(name="poker_table")
 * @ORM\Entity(repositoryClass="Game\Poker\CoreBundle\Repository\PokerTableRepository")
 */
class PokerTable
{

    const MAX_LIMIT_PLAYERS = 9 ;
    const TURN = 4 ;
    const RIVER = 5 ;


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id ;

    /**
     *
     * @ORM\ManyToMany(targetEntity="Game\Poker\CoreBundle\Entity\PokerUser", inversedBy="tables", cascade={"persist"})
     */
    private $players ;

    /**
     *
     * @ORM\OneToMany(targetEntity="Game\Poker\CoreBundle\Entity\GameSession", mappedBy="table", cascade={"persist"})
     */
    private $gameSessions ;

    /**
     * @ORM\ManyToOne(targetEntity="Game\Poker\CoreBundle\Entity\PokerUser")
     */
    private $currentPlayer ;

    /**
     * @ORM\OneToOne(targetEntity="Game\Poker\CoreBundle\Entity\PokerRound", cascade={"persist"})
     */
    private $round ;

    /**
     * @ORM\OneToOne(targetEntity="Game\Poker\CoreBundle\Entity\Deck", cascade={"persist"})
     */
    private $deck ;

    /**
     *
     * @ORM\OneToMany(targetEntity="Game\Poker\CoreBundle\Entity\BoardCard", mappedBy="table", cascade={"persist"})
     */
    private $boardCards ;

    /**
     * @ORM\ManyToOne(targetEntity="Game\Poker\CoreBundle\Entity\Game", inversedBy="tables", cascade={"persist"})
     */
    private $game ;

    /**
     * @var string
     *
     * @ORM\Column(name="pot", type="float", nullable=true)
     */
    private $pot ;

    /**
     * @var string
     *
     * @ORM\Column(name="gamelimit", type="string", length=255, nullable=true)
     */
    private $gamelimit;

    /**
     *
     * @ORM\Column(name="minBuyIn", type="float", nullable=true)
     */
    private $minBuyIn ;

    /**
     *
     * @ORM\Column(name="maxBuyIn", type="float", nullable=true)
     */
    private $maxBuyIn ;

    /**
     *
     * @ORM\Column(name="bigBlind", type="float", nullable=true)
     */
    private $bigBlind ;

    /**
     *
     * @ORM\Column(name="smallBlind", type="float", nullable=true)
     */
    private $smallBlind ;

    /**
     * @var string
     *
     * @ORM\Column(name="messages", type="array", nullable=true)
     */
    private $messages;

    /**
     * @var datetime
     *
     * @ORM\Column(name="dateTimeStart", type="datetime", nullable=true)
     */
    private $dateTimeStart;

    /**
     * @var datetime
     *
     * @ORM\Column(name="dateTimeEnd", type="datetime", nullable=true)
     */
    private $dateTimeEnd;


    private $dealer ;



    public function __construct( $gameLimit )
    {
        $this -> players = new ArrayCollection() ;
        $this -> deck = new Deck() ;
        $this -> gamelimit = $gameLimit ;

        $blinds = explode( '/' , $gameLimit ) ;

        $this -> bigBlind = ( float ) $blinds[ 1 ] ;
        $this -> smallBlind = ( float ) $blinds[ 0 ] ;

        $this -> maxBuyIn = 100 * $this -> bigBlind ;
        $this -> minBuyIn = 80  * $this -> smallBlind ;
    }


    public function isNew()
    {
        return ( null == $this -> getId() ) ? true : false ;
    }


    public function maxLimitPlayers()
    {
        return PokerTable::MAX_LIMIT_PLAYERS ;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this -> id ;
    }



    /**##########################################################################################**/
    /***************************************** GAME METHODS ***************************************/
    /**##########################################################################################**/


    /**
     * Set game
     *
     * @param Game $game
     *
     * @return PokerTable
     */
    public function setGame( Game $game = null )
    {
        $this -> game = $game ;

        return $this ;
    }

    /**
     * Get game
     *
     * @return Game
     */
    public function getGame()
    {
        return $this -> game ;
    }



    /**##########################################################################################**/
    /***************************************** ROUND METHODS **************************************/
    /**##########################################################################################**/

    /**
     * Set round
     *
     * @param integer $round
     *
     * @return Game
     */
    public function setRound( PokerRound  $round) {

        $this -> round = $round ;

        return $this ;
    }


    /**
     * Get round
     *
     * @return integer
     */
    public function getRound( $newRound = false )
    {

        if ( $this -> round == null OR $newRound )

                $this -> round = new Round() ;

        return $this -> round ;
    }



    /**##########################################################################################**/
    /***************************************** DECK METHODS ***************************************/
    /**##########################################################################################**/


    /**
     * Set deck
     *
     * @param $deck
     *
     * @return Game
     */
    public function setDeck( Deck $deck = null )
    {
        $this -> deck = $deck ;

        return $this ;
    }


    /**
     * Get deck
     *
     * @return \deck
     */
    public function getDeck()
    {
        return $this -> deck ;
    }


    /**##########################################################################################**/
    /************************************* BOARDCARDS METHODS *************************************/
    /**##########################################################################################**/


    /**
     * Get $cards
     *
     * @return $cards
     */
    public function getBoardCards()
    {
        return $this -> boardCards ;
    }


    /**
     * Set BoardCards
     *
     * @param $cards
     *
     * @return PokerTable
     */
    public function setBoardCards( $cards )
    {

        foreach( $cards as $card )
        {
            $this -> addBoardCard( $card ) ;
        }

        return $this ;
    }


    /**
     * Add boardCard
     *
     * @param BoardCard $card
     *
     * @return PokerTable
     */
    public function addBoardCard( BoardCard $card )
    {

        $card -> setTable( $this ) ;

        $this -> boardCards[] = $card ;

        return $this ;
    }


    /**
     * Remove BoardCard
     *
     * @param BoardCard $boardCard
     */
    public function removeBoardCard( BoardCard $card )
    {
        $this -> boardCards -> removeElement( $card ) ;
    }


    public function getTransformedBoardCard( $round = null , $range = false  )
    {
        $cards = array() ;

        if(  $round <= count( $this -> getBoardCards() ) )
        {
            if( $range )
            {
                $i = 0 ;

                while( $i < $round )
                {
                    $card = $this -> getBoardCards()[ $i  ] -> getCard() ;
                    $cards[] = [ $card -> getRank() , $card -> getSuit() ] ;
                    $i++ ;
                }
            }
            else
            {
                $card = $this -> getBoardCards()[ $round - 1 ] -> getCard() ;

                return [ [ $card -> getRank() , $card -> getSuit() ] ] ;
            }

        }

        return $cards ;

    }



    /**##########################################################################################**/
    /**************************************** DEALER METHODS **************************************/
    /**##########################################################################################**/



    public function getDealer()
    {
        if ( $this -> dealer == null )

            $this -> dealer = new Dealer() ;

        return
            $this -> dealer ;
    }


    public function setDealer( Dealer $dealer )
    {
        $this -> dealer = $dealer ;
    }


    /**##########################################################################################**/
    /***************************************** POT METHODS ****************************************/
    /**##########################################################################################**/


    /**
     * @return string
     */
    public function getPot()
    {
        return $this -> pot ;
    }


    /**
     * @param string $pot
     */
    public function setPot(  )
    {

        $pot = 0 ;

        foreach( $this -> players as $player )
        {
            $pot += $player -> getCurrentTableSession( $this ) -> getBet() ;

            $player -> getCurrentTableSession( $this ) -> resetBet( ) ;
        }

        if( null != $this -> pot )

            $this -> pot += $pot ;

        else

            $this -> pot = $pot ;

    }


    /**##########################################################################################**/
    /*************************************** PLAYERS METHODS **************************************/
    /**##########################################################################################**/


    /**
     * Add player
     *
     * @param \Game\Poker\CoreBundle\Entity\PokerUser $player
     *
     * @return PokerTable
     */
    public function addPlayer( PokerUser $player )
    {
        $this -> players[] = $player ;

        return $this ;
    }


    /**
     * Remove player
     *
     * @param \Game\Poker\CoreBundle\Entity\PokerUser $player
     */
    public function removePlayer( PokerUser $player )
    {
        $this -> players -> removeElement( $player ) ;
    }


    public function getPlayers()
    {
        return $this -> players ;
    }


    public function hasPlayer( PokerUser $player )
    {

        foreach( $this -> players as $tablePplayer )

            if( $tablePplayer -> getId() == $player -> getId() )

                return true ;


        return false ;

    }


    /**
     * Set currentPlayer
     *
     * @param string $currentPlayer
     *
     * @return Game
     */
    public function setCurrentPlayer( PokerUser $currentPlayer )
    {
        $this -> currentPlayer = $currentPlayer ;

        $this -> round -> setCurrentPlayer( $currentPlayer ) ;

        return $this ;
    }


    /**
     * Get currentPlayer
     *
     * @return string
     */
    public function getCurrentPlayer( $getPokerUserObject = false )
    {
        $currentRoundPlayer = $this -> round -> getCurrentPlayer()  ;

        if( null != $currentRoundPlayer)
        {
            if( $this -> currentPlayer -> getId() == $currentRoundPlayer -> getId() )

                return $this -> currentPlayer ;

            else
            {
                foreach( $this -> players as $player )
                {

                    if( $player -> getId() == $currentRoundPlayer -> getId() )
                    {
                        $this -> currentPlayer = $player ;

                        return $player ;
                    }
                    else

                        return ( $getPokerUserObject ) ? null : new PokerUser() ;

                }
            }
        }
        else
        {
            return current( $this -> players )[ 0 ] ;
        }

    }


    public function getFirstPlayer()
    {
        return $this -> getNthPlayer( 1 ) ;
    }


    public function getNthPlayer( $positionOnTable )
    {
        $positionInArray = $positionOnTable - 1 ;

        if( isset( $this -> players[ $positionInArray ] ) )

            return $this -> players[ $positionInArray ] ;

        else

            return null ;
    }


    public function getHowManyPlayers()
    {
        return  count( $this -> getPlayers() ) ;
    }


    public function getHowManyPlayersInPlay()
    {

        $howMany = 0 ;

        foreach ( $this -> players as $player )

            if( true == $player -> getCurrentTableSession( $this ) -> getIsInPlay() ) $howMany += 1 ;

        return  $howMany ;

    }


    public function getLastPlayerAdded()
    {
        return $this -> players[ count( $this -> players ) - 1 ] ;
    }


    public function getPlayer( $playerId , $getPokerUserObject = true )
    {
        if( null !== $playerId )
        {
            foreach ( $this -> getPlayers() as $player )

                if( $player -> getId() == $playerId )

                    return $player ;

                else

                    return ( $getPokerUserObject ) ? new PokerUser()  : null ;
        }
        else
            return ( ( $player = $this -> getCurrentPlayer() ) OR ( $player = $this -> getLastPlayerAdded() ) ) ? $player : new PokerUser() ;
    }


    public function everyBodyHasCards()
    {
        $isEverybodyServed = true ;

        foreach( $this -> players as $player )

            $isEverybodyServed = ( $isEverybodyServed ==  $player -> getCurrentTableSession( $this ) -> hasHand() ) ;

        return $isEverybodyServed;

    }


    public function everyBodyHasPlayed()
    {
        $hasEverybodyPlayed = true ;

        foreach( $this -> players as $player )

            $hasEverybodyPlayed = ( $hasEverybodyPlayed ==  $player -> getCurrentTableSession( $this ) -> getHasPlayed() ) ;

        return $hasEverybodyPlayed;

    }


    public function getPlayersChips()

    {
        $chips = array() ;

        foreach( $this -> players as $player )
        {
            $chips[ 'player-' . $player -> getId() ] = $player -> getChipsFor( $this ) ;
        }

        return $chips;
    }


    public function setPlayersInPlay()
    {

        foreach ( $this -> players as $player )

            $player -> getCurrentTableSession( $this ) -> setIsInPlay( true ) ;
    }


    public function getPlayersInPlay()
    {
        $playersInPlay = array() ;

        foreach ( $this -> players as $player )
        {
            $playersInPlay[ 'player-' . $player -> getId() ] = $player -> getCurrentTableSession( $this ) -> getIsInplay() ;
        }

        return $playersInPlay ;
    }


    public function getPlayersCurrentBet()
    {
        $playersCurrentBet = array() ;

        foreach ( $this -> players as $player )
        {
            $playersCurrentBet[ 'player-' . $player -> getId() ] = $player -> getCurrentTableSession( $this ) -> getBet() ;
        }

        return $playersCurrentBet ;
    }


    public function getNextValidPlayer( PokerUser $currentPlayer = null )
    {

        if( ! is_null( $currentPlayer ) )
        {
            $currentPlayerId = $currentPlayer -> getId() ;
        }
        else if( ! is_null( $this -> getCurrentPlayer() ) )
        {
            $currentPlayerId = $this -> getCurrentPlayer() -> getId() ;
        }
        else if( ! is_null( $this -> round ->  getSmallBlindPlayer() ) )
        {
            $currentPlayerId = $this -> round ->  getSmallBlindPlayer() -> getId() ;
        }
        else
        {
            $currentPlayerId = current( $this -> players )[ 0 ] ;
        }

        $currentSitNumber = null ;
        $nextValidPlayer = null ;

        foreach ( $this -> players as $sitNumber => $player )
        {
            if( $player -> getId() == $currentPlayerId ) $currentSitNumber = $sitNumber ;
        }

        while( ! $nextValidPlayer )
        {
            if( ! isset( $this -> players[ $currentSitNumber + 1 ] ) )
            {
                $currentSitNumber = -1 ;

                continue ;
            }

            if( $this -> players[ $currentSitNumber + 1 ] ->  getCurrentTableSession( $this ) -> getisInPlay() )
            {
                $nextValidPlayer = $this -> players[ $currentSitNumber + 1 ]  ;
            }
            else

                $currentSitNumber += 1 ;
        }

        return $nextValidPlayer ;
    }


    public function setBlindsPlayers()
    {

        if( null == $this -> currentPlayer )
        {

            $currentSmallBlindPlayer = current( $this -> players )[ 0 ] ;

        }
        else
        {

            $currentSmallBlindPlayer = $this -> getNextValidPlayer() ;

        }

        $this -> setCurrentPlayer( $currentSmallBlindPlayer ) ;

        $this -> round -> setSmallBlindPlayer( $this , $currentSmallBlindPlayer , $this -> getSmallBlind() ) ;

        $this -> round -> setBigBlindPlayer( $this , $this -> getNextValidPlayer() , $this -> getBigBlind() ) ;

    }


    public function hasEqualBets()
    {

        $hasEqualBets = true ;
        $start = true ;

        foreach( $this -> players as $player )
        {

           if( $start )
           {
               $lastBet =  $player -> getCurrentTableSession( $this ) -> getBet() ;

               $start = false ;

               continue ;

           }

            $currentBet = $player -> getCurrentTableSession( $this ) -> getBet() ;

            $hasEqualBets = $hasEqualBets && ( $lastBet == $currentBet ) ;

            $lastBet = $currentBet ;

        }

        return $hasEqualBets ;
    }


    /**##########################################################################################**/
    /*************************************** SESSIONS METHODS *************************************/
    /**##########################################################################################**/



    /**
     * Add gameSession
     *
     * @param \Game\Poker\CoreBundle\Entity\GameSession $gameSession
     *
     * @return PokerTable
     */
    public function addGameSession( GameSession $gameSession )
    {
        $gameSession -> setTable( $this ) ;

        $this -> gameSessions[] = $gameSession ;

        return $this ;
    }

    /**
     * Remove gameSession
     *
     * @param \Game\Poker\CoreBundle\Entity\GameSession $gameSession
     */
    public function removeGameSession( GameSession $gameSession )
    {
        $gameSession -> setTable( null ) ;

        $this -> gameSessions -> removeElement( $gameSession ) ;
    }

    /**
     * Get gameSessions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGameSessions()
    {
        return $this -> gameSessions ;
    }


    /**
     * Set gamelimit
     *
     * @param string $gamelimit
     *
     * @return PokerTable
     */
    public function setGamelimit( $gamelimit )
    {
        $this -> gamelimit = $gamelimit ;

        return $this ;
    }

    /**
     * Get gamelimit
     *
     * @return string
     */
    public function getGamelimit()
    {
        return $this -> gamelimit ;
    }

    /**
     * Set minBuyIn
     *
     * @param integer $minBuyIn
     *
     * @return PokerTable
     */
    public function setMinBuyIn( $minBuyIn )
    {
        $this -> minBuyIn = $minBuyIn ;

        return $this ;
    }

    /**
     * Get minBuyIn
     *
     * @return integer
     */
    public function getMinBuyIn()
    {
        return $this -> minBuyIn ;
    }

    /**
     * Set maxBuyIn
     *
     * @param integer $maxBuyIn
     *
     * @return PokerTable
     */
    public function setMaxBuyIn( $maxBuyIn )
    {
        $this -> maxBuyIn = $maxBuyIn ;

        return $this ;
    }

    /**
     * Get maxBuyIn
     *
     * @return integer
     */
    public function getMaxBuyIn()
    {
        return $this -> maxBuyIn ;
    }

    /**
     * Set bigBlind
     *
     * @param integer $bigBlind
     *
     * @return PokerTable
     */
    public function setBigBlind( $bigBlind )
    {
        $this -> bigBlind = $bigBlind ;

        return $this ;
    }

    /**
     * Get bigBlind
     *
     * @return integer
     */
    public function getBigBlind()
    {
        return $this -> bigBlind ;
    }

    /**
     * Set smallBlind
     *
     * @param integer $smallBlind
     *
     * @return PokerTable
     */
    public function setSmallBlind( $smallBlind )
    {
        $this -> smallBlind = $smallBlind ;

        return $this ;
    }

    /**
     * Get smallBlind
     *
     * @return integer
     */
    public function getSmallBlind()
    {
        return $this -> smallBlind ;
    }


    /**
     * Set messages
     *
     * @param string $message
     *
     * @return PokerTable
     */
    public function addMessage( $message )
    {
        $this -> messages[] = $message ;

        return $this;
    }


    /**
     * Get messages
     *
     * @return array
     */
    public function getMessages()
    {
        return $this -> messages ;
    }


    public function resetMessages()
    {
        $this -> messages = array() ;
    }


    /**
     * Set dateTimeStart
     *
     * @param string $dateTimeStart
     *
     * @return PokerTable
     */
    public function setDateTimeStart()
    {
        $this -> dateTimeStart = new \DateTime() ;

        return $this ;
    }


    /**
     * Get dateTimeStart
     *
     * @return string
     */
    public function getDateTimeStart()
    {
        return $this -> dateTimeStart ;
    }


    /**
     * Set dateTimeEnd
     *
     * @param string $dateTimeEnd
     *
     * @return PokerTable
     */
    public function setDateTimeEnd()
    {
        $this -> dateTimeEnd = new \DateTime() ;

        return $this ;
    }


    /**
     * Get dateTimeEnd
     *
     * @return string
     */
    public function getDateTimeEnd()
    {
        return $this -> dateTimeEnd ;
    }

}
