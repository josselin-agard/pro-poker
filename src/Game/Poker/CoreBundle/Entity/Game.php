<?php

namespace Game\Poker\CoreBundle\Entity;

use Doctrine\DBAL\Schema\Table;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Game\Poker\CoreBundle\Entity\PokerTable;

/**
 * Game
 *
 * @ORM\Table(name="game")
 * @ORM\Entity(repositoryClass="Game\Poker\CoreBundle\Repository\GameRepository")
 */
class Game {

    const FLOP = 3 ;
    const TURN = 4 ;
    const RIVER = 5 ;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="variant", type="string", length=255)
     */
    private $variant;

    /**
     * @var string
     *
     * @ORM\Column(name="mode", type="string", length=255)
     */
    private $mode;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var datetime
     *
     * @ORM\Column(name="dateTimeStart", type="datetime", nullable=true)
     */
    private $dateTimeStart;

    /**
     * @var datetime
     *
     * @ORM\Column(name="dateTimeEnd", type="datetime", nullable=true)
     */
    private $dateTimeEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="gamelimit", type="string", length=255, nullable=true)
     */
    private $gamelimit;

    /**
     * @ORM\OneToMany(targetEntity="Game\Poker\CoreBundle\Entity\PokerTable", mappedBy="game", cascade={"persist"})
     */
    private $tables ;

    private $currentTable;

    public function __construct( $gameLimit = null )
    {
        $this -> gamelimit = $gameLimit ;

        $this -> dateTimeStart = new \DateTime() ;

        $this -> tables = new ArrayCollection() ;

        $this -> addTable( new PokerTable( $gameLimit ) ) ;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set variant
     *
     * @param string $variant
     *
     * @return Game
     */
    public function setVariant($variant) {
        $this->variant = $variant;

        return $this;
    }

    /**
     * Get variant
     *
     * @return string
     */
    public function getVariant() {
        return $this->variant;
    }

    /**
     * Set mode
     *
     * @param string $mode
     *
     * @return Game
     */
    public function setMode($mode) {
        $this->mode = $mode;

        return $this;
    }

    /**
     * Get mode
     *
     * @return string
     */
    public function getMode() {
        return $this->mode;
    }


    /**
     * Set type
     *
     * @param string $type
     *
     * @return Game
     */
    public function setType($type) {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Set dateTimeStart
     *
     * @param string $dateTimeStart
     *
     * @return Game
     */
    public function setDateTimeStart($dateTimeStart) {
        $this->dateTimeStart = $dateTimeStart;

        return $this;
    }

    /**
     * Get dateTimeStart
     *
     * @return string
     */
    public function getDateTimeStart() {
        return $this->dateTimeStart;
    }

    /**
     * Set dateTimeEnd
     *
     * @param string $dateTimeEnd
     *
     * @return Game
     */
    public function setDateTimeEnd($dateTimeEnd) {
        $this->dateTimeEnd = $dateTimeEnd;

        return $this;
    }

    /**
     * Get dateTimeEnd
     *
     * @return string
     */
    public function getDateTimeEnd() {
        return $this->dateTimeEnd;
    }

    /**
     * Set gamelimit
     *
     * @param string $gamelimit
     *
     * @return Game
     */
    public function setGameLimit($limit) {

        $this->gamelimit = $limit ;

        return $this;
    }

    /**
     * Get gamelimit
     *
     * @return string
     */
    public function getGameLimit()
    {
        return $this->gamelimit;
    }


    /**
     * Add table
     *
     * @param \Game\Poker\CoreBundle\Entity\PokerTable $table
     *
     * @return Game
     */
    public function addTable( PokerTable $table )
    {
        $table -> setGame( $this ) ;
        $this -> tables[] = $table ;

        return $this ;
    }


    /**
     * Remove table
     *
     * @param \Game\Poker\CoreBundle\Entity\PokerTable $table
     */
    public function removeTable( PokerTable $table )
    {
        $table -> setGame ( null ) ;
        $this -> tables -> removeElement( $table ) ;
    }


    /**
     * Get tables
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTables()
    {
        return $this -> tables ;
    }


    public function getCurrentTable()
    {
        return $this -> currentTable ;
    }


    public function setCurrentTable( PokerTable $table )
    {
        $this -> currentTable = $table ;

        return $this ;
    }


    public function getLastTableAdded()
    {
        return $this -> tables[ count( $this -> tables ) - 1 ] ;
    }


    public function getTable( $tableId = 0 )
    {
        if( 0 != $tableId )
        {
            foreach ( $this -> getTables() as $table )

                if( $table -> getId() == $tableId )
                {
                    $this -> setCurrentTable( $table ) ;

                    return $table ;
                }
                else

                    return new Table() ;
        }
        else
            return ( ( $table = $this -> getCurrentTable() ) OR ( $table = $this -> getLastTableAdded() ) ) ? $table : new Table() ;
    }


    public function hasNoValidTableFor( PokerUser $player )
    {
        $hasNotPlayer = true ;
        $hasTooManyPlayers = true ;


        foreach ( $this -> tables as $table )
        {

            $hasNotPlayer = ( $hasNotPlayer == ! $table -> hasPlayer( $player ) ) ;

            $hasTooManyPlayers = ( $hasTooManyPlayers == ( $table -> maxLimitPlayers() <= $table -> getHowManyPlayers() ) ) ;

        }

        return $hasNotPlayer AND $hasTooManyPlayers ;

    }


    public function currentInfos( )

    {
        $Table = $this -> getCurrentTable() ;
        $Round = $Table -> getRound() ;

        return [
                    'variant' => $this -> getVariant() ,

                    'mode'    => $this -> getMode() ,

                    'players' =>   [

                                            'currentPlayer' => $Table -> getCurrentPlayer() -> getId() ,

                                            'allowedPlayer' => ( null == $Round -> getAllowedPlayer() ) ? 'locked' : $Round -> getAllowedPlayer() -> getId() ,

                                            'playersChips'  => $Table -> getPlayersChips() ,

                                            'playersInPlay'  => $Table -> getPlayersInPlay() ,

                                            'playersCurrentBet'  => $Table -> getPlayersCurrentBet() ,

                                            'playersNb'     => $Table -> getHowManyPlayers() ,

                                        ] ,


                    'table'   =>   [

                                            'message' => $Table -> getMessages() ,

                                            'pot'  => $Table -> getPot() ,

                                            'board' =>  [

                                                            'flop'  => $Table -> getTransformedBoardCard( self::FLOP , true )  ,

                                                            'turn'  => $Table -> getTransformedBoardCard( self::TURN )  ,

                                                            'river' => $Table -> getTransformedBoardCard( self::RIVER ) ,

                                                        ] ,

                                            'round' =>  [

                                                            'bigBlind' => ( null !=  $Round -> getBigBlindPlayer() ) ? $Round -> getBigBlindPlayer() -> getId() : null ,

                                                            'smallBlind' => ( null !=  $Round -> getSmallblindPlayer() ) ? $Round -> getSmallblindPlayer() -> getId() : null  ,

                                                            'isComplete' => $Round -> getIsComplete() ,

                                                            'isReady' => $Round -> getIsReady() ,

                                                            'isWaiting' => $Round -> getIsWaiting() ,

                                                            'isGameStarted' => $Round -> getIsGameStarted() ,

                                                            'isPlayTime' => $Round -> getIsPlayTime() ,

                                                            'betsAreEqual' => $Round -> getbetsAreEqual() ,

                                                            'current' => $Round -> getName() ,

                                                            'lastBet' => $Round -> getCurrentBet()

                                                        ] ,

                                        ]

                ] ;
    }
}
