<?php

namespace Game\Poker\CoreBundle\Entity ;

use Doctrine\Common\Collections\ArrayCollection ;
use Doctrine\ORM\Mapping as ORM ;

use TWS\UserBundle\Entity\User ;
use Game\Poker\CoreBundle\Traits\PokerPlayer ;

use Game\Poker\CoreBundle\Entity\GameSession ;
use Game\Poker\CoreBundle\Entity\PokerTable ;

/**
 * PokerUser
 *
 * @ORM\Table(name="poker_user")
 * @ORM\Entity(repositoryClass="Game\Poker\CoreBundle\Repository\PokerUserRepository")
 */
class PokerUser extends User
{

    use PokerPlayer ;

    private $rewards ;
    private $statistics ;


    public function __construct()
    {
        parent::__construct() ;

        $this -> tables = new ArrayCollection() ;
        $this -> bankroll = 1000 ;

        $this->roles = array('ROLE_USER');
    }
    

    function getRewards()
    {
        return $this -> rewards ;
    }

    function getStatistics()
    {
        return $this -> statistics ;
    }


    function setRewards($rewards)
    {
        $this->rewards = $rewards ;
    }

    function setStatistics( $statistic )
    {
        $this -> statistic = $statistic ;
    }

}
