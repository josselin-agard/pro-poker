<?php

namespace Game\Poker\CoreBundle\Entity ;

use Doctrine\ORM\Mapping as ORM ;

use Game\Poker\CoreBundle\Entity\Hand ;
use Game\Poker\CoreBundle\Entity\PokerTable ;
use Game\Poker\CoreBundle\Entity\PokerUser ;

/**
 * GameSession
 *
 * @ORM\Table(name="game_session")
 * @ORM\Entity(repositoryClass="Game\Poker\CoreBundle\Repository\GameSessionRepository")
 */
class GameSession
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\Column(name="buyIn", type="float", nullable=true)
     */
    private $buyIn;


    /**
     *
     * @ORM\Column(name="bet", type="float", nullable=true)
     */
    private $bet;

    /**
     *
     * @ORM\Column(name="isInPlay", type="boolean", nullable=true)
     */
    private $isInPlay;

    /**
     *
     * @ORM\Column(name="hasPlayed", type="boolean", nullable=true)
     */
    private $hasPlayed;

    /**
     *
     * @ORM\Column(name="lastAction", type="string", length=255, nullable=true)
     */
    private $previousAction;

    /**
     * @ORM\OneToOne(targetEntity="Game\Poker\CoreBundle\Entity\Hand", cascade={"persist"})
     */
    private $hand;

    /**
     * @ORM\ManyToOne(targetEntity="Game\Poker\CoreBundle\Entity\PokerTable", inversedBy="gameSessions", cascade={"persist"})
     */
    private $table;

    /**
     * @ORM\ManyToOne(targetEntity="Game\Poker\CoreBundle\Entity\PokerUser", inversedBy="gameSessions", cascade={"persist"})
     */
    private $player;


    public function __construct()
    {
        $this->hand = new Hand();

        $this->isInPlay = false;

        $this->hasPlayed = false;
    }


    public function isNew()
    {
        return (null == $this->getId()) ? true : false;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set buyIn
     *
     * @param integer $buyIn
     *
     * @return GameSession
     */
    public function setbuyIn($buyIn)
    {
        $this->buyIn = $buyIn;

        return $this;
    }

    /**
     * Get buyIn
     *
     * @return integer
     */
    public function getbuyIn()
    {
        return $this->buyIn;
    }

    /**
     * Set bet
     *
     * @param integer $bet
     *
     * @return GameSession
     */
    public function setBet($bet)
    {
        if (null != $this->bet)

            $this->bet += $bet;

        else

            $this->bet = $bet;

        return $this;
    }

    /**
     * Get bet
     *
     * @return integer
     */
    public function getBet()
    {
        return $this -> bet ;
    }


    public function resetBet()
    {
        $this -> bet = 0 ;
    }

    /**
     * Set isInPlay
     *
     * @param boolean $isInPlay
     *
     * @return GameSession
     */
    public function setIsInPlay($isInPlay)
    {
        $this->isInPlay = $isInPlay;

        return $this;
    }

    /**
     * Get isInPlay
     *
     * @return boolean
     */
    public function getIsInPlay()
    {
        return $this->isInPlay;
    }

    /**
     * Set lastAction
     *
     * @param string $previousAction
     *
     * @return GameSession
     */
    public function setPreviousAction($previousAction)
    {
        $this->previousAction = $previousAction;

        return $this;
    }

    /**
     * Get previousAction
     *
     * @return string
     */
    public function getPreviousAction()
    {
        return $this->previousAction;
    }


    public function recieveHand(Array $cards)
    {

        foreach ($cards as $card) {
            $this->hand->addCard($card);
        }

    }


    public function isEmpty()
    {
        return (null == $this->getId()) ? true : false;
    }


    /**
     * Set hand
     *
     * @param \Game\Poker\CoreBundle\Entity\Hand $hand
     *
     * @return GameSession
     */
    public function setHand(Hand $hand = null)
    {
        $this->hand = $hand;

        return $this;
    }

    /**
     * Get hand
     *
     * @return \Game\Poker\CoreBundle\Entity\Hand
     */
    public function getHand()
    {
        return $this->hand;
    }

    public function hasHand()
    {
        return !$this->hand->isEmpty();
    }

    /**
     * Set table
     *
     * @param \Game\Poker\CoreBundle\Entity\PokerTable $table
     *
     * @return GameSession
     */
    public function setTable(PokerTable $table = null)
    {
        $this->table = $table;

        return $this;
    }

    /**
     * Get table
     *
     * @return \Game\Poker\CoreBundle\Entity\PokerTable
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * Set player
     *
     * @param \Game\Poker\CoreBundle\Entity\PokerUser $player
     *
     * @return GameSession
     */
    public function setPlayer(PokerUser $player = null)
    {
        $this->player = $player;

        if ($player == null)

            $this->getTable()->removeGameSession($this);

        else

            $player->getLastAddedTable()->addGameSession($this);

        return $this;
    }

    /**
     * Get player
     *
     * @return \Game\Poker\CoreBundle\Entity\PokerUser
     */
    public function getPlayer()
    {
        return $this->player;
    }


    public function currentInfos()
    {
        return [

            'myId' => $this->getPlayer()->getId(),

            'myHand' => $this->getHand()->getTransformedHand(),

            'isInPlay' => $this->getIsInPlay(),

            'bet' => $this->getBet()

        ];
    }


    /**
     * Set hasPlayed
     *
     * @param boolean $hasPlayed
     *
     * @return GameSession
     */
    public function setHasPlayed($hasPlayed)
    {
        $this->hasPlayed = $hasPlayed;

        return $this;
    }


    /**
     * Get hasPlayed
     *
     * @return boolean
     */
    public function getHasPlayed()
    {
        return $this->hasPlayed;
    }

}
