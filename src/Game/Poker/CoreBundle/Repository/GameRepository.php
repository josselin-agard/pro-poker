<?php

namespace Game\Poker\CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * GameRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class GameRepository extends \Doctrine\ORM\EntityRepository {

    public function gameAvailable($mode, $blind) {

        $qb = $this->createQueryBuilder('a');
        $qb
                ->where('a.variant = :variant')
                ->setParameter('variant', $mode)
                ->andWhere('a.gamelimit = :blind')
                ->setParameter('blind', $blind)
                ->setMaxResults(1)
        ;
        return $qb
                        ->getQuery()
                        //Retourne 1 élément
                        ->getOneOrNullResult()
        ; //Tableau d'entité game
    }

   /* public function hasPlayer($gameId, $playerId) {
        $qb = $this->createQueryBuilder('g');
        $qb
             
                ->leftJoin('g.players', 'p')
                ->where('p.id = :playerId')
                ->setParameter('playerId', $playerId)
                ->andWhere('g.id = :gameId')
                ->setParameter('gameId', $gameId)

        ;
        $result = $qb->getQuery()->getOneOrNullResult();

        return ( empty($result) ) ? false : true;
    } */

}
