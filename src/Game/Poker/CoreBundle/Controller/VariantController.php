<?php

namespace Game\Poker\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\HttpFoundation\Request;

use Game\Poker\CoreBundle\Entity\VariantParameters;

class VariantController extends Controller
{

    private $routePrefix = "poker_" ;
    
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this -> redirectToRoute( 'variant' ) ;
    }

    /**
     * @Route("/poker", name = "variant")
     */
    public function variantAction( Request $request )
    {

        $variant = new VariantParameters() ;

        $formBuilder = $this -> get('form.factory') -> createBuilder(FormType::class , $variant) ;

        $formBuilder
                    -> setAction( $this->generateUrl( 'variant' ) )

                    -> add( 'name' , 'choice' , array(
                                                        'choices' => array(
                                                                            'texas-holdem' => 'Texas Holdem',
                                                                         )
                                                     ))

                    -> add( 'mode' , 'choice' , array(
                                                        'multiple' => false ,
                                                        'expanded' => true ,
                                                        'choices' => array(
                                                        'solo' => 'Mode Solo ',
                                                        'multi' => 'Mode Multi-joueurs' ,
                                                    )
                            ))

                    -> add('nbPlayers', 'choice', array(
                                                            'choices' => array(
                                                                '2' => 2,
                                                                '3' => 3,
                                                                '4' => 4,
                                                                '5' => 5,
                                                                '6' => 6,
                                                                '7' => 7,
                                                                '8' => 8,
                                                            )
                    ))
        ;

        $form = $formBuilder -> getForm() ;

        if ( $request -> getMethod() == 'POST' )
        {
            $form -> bind( $request ) ;

            if ( $form -> isValid() )
            {
                $gameAttribut[ 'nbPlayers' ] = $variant -> getNbPlayers() ;
                
                return $this -> redirectToRoute( $this -> routePrefix . $variant -> getName() . '_' . $variant -> getMode() , $gameAttribut ) ;
            }
        }

        return $this -> render( 'GamePokerCoreBundle:variant:variant.html.twig' , array(
                                                                                            'form' => $form -> createView() ,
        )) ;
    }

}
