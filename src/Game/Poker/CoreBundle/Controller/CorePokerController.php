<?php

namespace Game\Poker\CoreBundle\Controller ;


use Symfony\Bundle\FrameworkBundle\Controller\Controller ;
use Symfony\Component\HttpFoundation\Request ;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route ;
use \Symfony\Component\HttpFoundation\JsonResponse ;

use Game\Poker\CoreBundle\Traits\CommonPokerActions ;

use Game\Poker\CoreBundle\Entity\Game ;
use Game\Poker\CoreBundle\Models\BuyIn ;

require_once __DIR__ . '/../Resources/ErrorsConstants.php' ;


class CorePokerController extends Controller
{

    /* Quelques constantes de Class utiles pour comparer les phases du jeu plus facilement */

    const INIT  = 0 ;
    const FLOP  = 1 ;
    const TURN  = 2 ;
    const RIVER = 3 ;

    const NEW_ROUND = 1 ;


    /* Un Trait pour les méthodes de vérifications courante dans un jeu de poker */

    use CommonPokerActions ;


    /* Valeurs d'initialisation de l'entité Game, propre à chaque controlleur, méthode  */

    protected $variant ;
    protected $mode ;
    protected $type ;


    /* Méthode dont le simple but est de rendre accessible les principales entités du jeu plus facilement */

    public function hydrate( Game $game , $tableId )
    {

        $this -> game    =  $game ;

        $this -> table   =  $this -> game -> getTable( $tableId ) ;

        $this -> dealer  =  $this -> table -> getDealer() ;

        $this -> deck    =  $this -> table -> getDeck() ;

        $this -> round   =  $this -> table -> getRound  (
                                                            ( ( $this -> table -> isNew()  ) ? self::NEW_ROUND : false )
                                                        ) ;

    }


    /* Méthode pour la création d'un nouveau jeu */

    public function createGame( $blind )
    {

        $cards = $this  -> getDoctrine()

                        -> getManager()

                        -> getRepository( 'GamePokerCoreBundle:Card' ) -> findAll() ;


        $game =  new Game( $blind ) ;

        $game -> setVariant( $this -> variant ) ;

        $game -> setMode( $this -> mode ) ;

        $game -> getTable() -> getDeck() -> setCards( $cards ) ;

        return $game ;

    }


    public function initializeGame( $gameId , $tableId )
    {

        $this -> em     =  $this -> getDoctrine() -> getManager() ;
        $this -> player =  $this -> getUser() ;

        if( 0 != $gameId )
        {

            $game = $this -> getDoctrine()

                          -> getRepository( 'GamePokerCoreBundle:Game' )

                          -> find( $gameId ) ;

        }
        else
        {

            //Jetons du joueur connecté

            $bankroll = $this -> player -> getBankroll() ;

            //Sélectionner le niveau de blind

            $blind = ( new BuyIn( $bankroll ) ) -> getLimit() ;


            //Récupère entity game avec la variante et niveau de blind

            $game = $this -> em -> getRepository( 'GamePokerCoreBundle:Game' )

                                -> gameAvailable( $this -> variant , $blind ) ;


        }

        /* Si on ne récupère pas de jeu OU qu'il ne possède pas de tables remplissant les coniditions nécessaires à
           l'ajout d'un joueur, on crée un nouveau jeu */

        if  ( empty( $game ) OR $game -> hasNoValidTableFor( $this -> player ) )

            $game = $this -> createGame( $blind ) ;


        $this -> hydrate( $game , $tableId ) ;

        if( null == $game -> getId() AND null == $this -> round -> getAllowedPlayer() )

            $this -> round -> setAllowedPlayer( $this -> player ) ;

        /* C'est la méthode importante, c'est là qu'on démarre la session à une table */

        $this -> gameSession = $this -> player -> sitDownAt( $this -> table ) ;

        $this -> em -> persist( $this -> game ) ;

        $this -> em -> flush() ;

    }


    /**
     * @Route("/poker/refresh/game/{gameId}/table/{tableId}", name="poker_refresh")
     */
    public function refreshGameAction( $gameId , $tableId )
    {
        $this -> initializeGame( $gameId , $tableId ) ;

        return new JsonResponse( [

                                    'game'   => $this -> game        -> currentInfos() ,

                                    'player' => $this -> gameSession -> currentInfos()

                                 ] ) ;
    }


    public function refreshRound( $gameId , $tableId )
    {
        $this -> initializeGame( $gameId , $tableId ) ;

        $this -> round -> setAllowedPlayer( null ) ;

        $this -> em -> flush() ;

        $this -> table -> resetMessages() ;

    }


    /**
     * @Route("/poker/game/{gameId}/table/{tableId}/start", name="poker_start_game")
     */
    public function startGameAction( $gameId , $tableId )
    {

        $this -> refreshRound( $gameId , $tableId ) ;

        $this -> round -> setIsGameStarted( true ) ;

        $this -> table -> setDateTimeStart() ;

        $this -> table -> addMessage( 'Début de la partie.' ) ;

        $this -> table -> setPlayersInPlay() ;

        $this -> table -> addMessage( $this -> table -> getHowManyPlayersInPlay() . ' joueurs dans la partie.' ) ;

        $this -> em -> flush() ;

        return new JsonResponse( [ 'status' => 'done' , 'messages' =>  $this -> table -> getMessages() , 'nextAction' => array( 'url' => 'set-blinds' , 'match' => 'globalMatch' , 'message' => 'Placement des blinds...' ) ] ) ;

    }


    /**
     * @Route("/poker/game/{gameId}/table/{tableId}/set-blinds", name="poker_set_blinds")
     */
    public function setBlindsAction( $gameId , $tableId )
    {

        $this -> refreshRound( $gameId , $tableId ) ;

        $this -> table -> setBlindsPlayers() ;

        $this -> em -> flush() ;

        $this -> table -> addMessage( 'Blinds placées...' ) ;

        $this -> round -> setIsPlayTime( true ) ;

        $this -> round -> setAllowedPlayer( $this -> table -> getNextValidPlayer() ) ;

        $this -> em -> flush() ;

        return new JsonResponse( [ 'status' => 'done' , 'messages' =>  $this -> table -> getMessages() , 'nextAction' => array() ] ) ;

    }


   /*public function roundFilterAccess( $filter )
    {
        
        $isValide = true;

        $conditions = [
            'Init' => [],
            'IsValidUser' => [
                'playersBelongsToTable' => [
                    'hasParameters' => true,
                    'parameters' => []
                ] ,
                'isCurrentPlayer'

            ],
            'DealCards' => [
                'gameIsNotStarted',
                'hasEnoughPlayers'
                ],
            'Flop' => [],
            'Turn' => [],
            'River' => [],
            'ShowDown' => [],
            'Bet' => []
        ];

        foreach ($conditions[$filter] as $fonction => $options )
        {
            if( $options[ 'hasParameters' ] )
            {
                $isValide = ( $isValide == $this -> $fonction( $parameters ) ) ;
            }
            else
            {
                $isValide = ( $isValide == $this -> $fonction() ) ;
            }
        }

        return $isValide ;
    }*/

}
