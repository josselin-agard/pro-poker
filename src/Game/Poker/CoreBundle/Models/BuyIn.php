<?php
/**
 * Created by PhpStorm.
 * User: jo
 * Date: 31/01/17
 * Time: 12:05
 */

namespace Game\Poker\CoreBundle\Models;

class BuyIn
{
    private $limits = array
    (
        '8000' =>  [ 'sb' => 1,
                     'bb' => 2 ] ,

        '3000' =>  [ 'sb' => 0.5,
                     'bb' => 1 ] ,

        '1250' =>  [ 'sb' => 0.25,
                     'bb' => 0.5 ] ,

        '500' =>  [ 'sb' => 0.1,
                    'bb' => 0.25 ] ,

        '200' => [ 'sb' => 0.05,
                   'bb' => 0.1 ] ,

        '100' => [ 'sb' => 0.02,
                   'bb' => 0.05 ] ,

        '40' => [ 'sb' => 0.01,
                  'bb' => 0.02 ]
    ) ;

    private $limit ;


    public function __construct( $buyin )
    {
        foreach ( $this -> limits as $minBankroll => $blind )
        {
            if ( (int) $minBankroll < (int) $buyin )
            {
               $this -> setLimit( $blind ) ;
               break ;
            }
        }

        if( null == $this -> limit )

            throw new \Exception( 'NOT_ENOUGH_MONEY' ) ;
    }


    public function getLimits()
    {
        return $this -> limits ;
    }


    public function addLimit( $limit )
    {
        $this -> limits[] = $limit ;
    }


    public function getLimit( $converted = true )
    {
        if( $converted )

            return implode( '/' , $this -> limit ) ;

        else

            return $this -> limit ;
    }


    public function setLimit( $limit )
    {
        $this -> limit = $limit ;
    }

}