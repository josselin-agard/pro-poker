<?php
/**
 * Created by PhpStorm.
 * User: jo
 * Date: 31/01/17
 * Time: 12:05
 */

namespace Game\Poker\CoreBundle\Models;

use Game\Poker\CoreBundle\Entity\Deck;
use Game\Poker\CoreBundle\Entity\Hand;
use Game\Poker\CoreBundle\Entity\GameSession;

class Dealer
{
    public function dealCardsTo( GameSession $player , Deck $deck )
    {
        $player -> recieveHand( $deck -> extractCard() ) ;
    }

    public function dealBoardCards( Deck $deck , $howMany )
    {

           return  $deck -> extractCard( $howMany , true ) ;
    }

    public function transformCards( $cards )
    {

        $transformedCards = array() ;

        foreach ( $cards as $card )
        {
            $transformedCards[] = [ $card -> getRank() , $card -> getSuit() ] ;
        }

        return $transformedCards ;
    }

}