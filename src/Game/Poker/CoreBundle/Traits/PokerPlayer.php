<?php

namespace Game\Poker\CoreBundle\Traits ;

use ClassesWithParents\G ;

use Game\Poker\CoreBundle\Entity\GameSession ;
use Game\Poker\CoreBundle\Entity\PokerTable ;

trait PokerPlayer
{

    /**
     * @var string
     *
     * @ORM\Column(name="picture", type="string", length=255, nullable=true)
     */
    private $picture ;

    /**
     * @var int
     *
     * @ORM\Column(name="bankroll", type="integer", nullable=true)
     */
    private $bankroll ;

    /**
     *
     * @ORM\OneToMany(targetEntity="Game\Poker\CoreBundle\Entity\GameSession", mappedBy="player", cascade={"persist"})
     */
    private $gameSessions ;

    /**
     *
     * @ORM\ManyToMany(targetEntity="Game\Poker\CoreBundle\Entity\PokerTable", mappedBy="players", cascade={"persist"})
     */
    private $tables ;


    /**
     * Set picture
     *
     * @param string $picture
     *
     * @return PokerUser
     */
    public function setPicture( $picture )
    {
        $this -> picture = $picture ;

        return $this ;
    }


    /**
     * Get picture
     *
     * @return string
     */
    public function getPicture()
    {
        return $this -> picture ;
    }


    /**
     * Set bankroll
     *
     * @param integer $bankroll
     *
     * @return PokerUser
     */
    public function setBankroll( $bankroll )
    {
        $this -> bankroll = $bankroll ;

        return $this ;
    }


    /**
     * Get bankroll
     *
     * @return integer
     */
    public function getBankroll()
    {
        return $this -> bankroll ;
    }


    public function getCash( $howMany )
    {
        $this -> bankroll -= $howMany ;

        return $howMany ;
    }

    public function betChipsOn( PokerTable $table , $howMany )
    {
        $gameSession = $this -> getCurrentTableSession( $table ) ;

        $chips = $gameSession -> getbuyIn() ;

        if( $chips < $howMany )
        {
            $howMany = $chips ;
            $chips = 0 ;
        }
        else

           $chips -= $howMany ;

        $gameSession -> setbuyIn( $chips ) ;

        $gameSession -> setBet( $howMany ) ;

        $gameSession -> getTable() -> getRound() -> setCurrentBet( $gameSession -> getBet( )  ) ;

    }



    public function makeCashWithDrawOf( $buyIn , GameSession $gameSession = null )
    {
        if( null == $gameSession )

            $gameSession = $this -> getLastAddedGameSession() ;

        if(  0 <= ( $this -> bankroll ) - $buyIn )

            $gameSession -> setBuyIn ( $this -> getCash( $buyIn ) ) ;

        elseif( 0 <= ( $this -> bankroll ) - ( $this -> getLastAddedTable() -> getMinBuyIn() ) )

            $gameSession -> setBuyIn ( $this -> getCash( $buyIn ) ) ;

        else

            throw new \Exception( 'NOT_ENOUGH_MONEY' ) ;

    }


    public function getChipsFor( PokerTable $table )
    {
        return $this -> getCurrentTableSession( $table ) -> getbuyIn() ;
    }


    public function isInPlayOn( PokerTable $table )
    {
        return $this -> getCurrentTableSession( $table ) -> getIsInPlay() ;
    }


    /**
     * Add gameSession
     *
     * @param \Game\Poker\CoreBundle\Entity\GameSession $gameSession
     *
     * @return PokerUser
     */
    public function addGameSession( GameSession $gameSession )
    {
        $gameSession -> setPlayer( $this ) ;

        $this -> gameSessions[] = $gameSession ;

        return $this ;
    }


    /**
     * Remove gameSession
     *
     * @param \Game\Poker\CoreBundle\Entity\GameSession $gameSession
     */
    public function removeGameSession( GameSession $gameSession )
    {
        $gameSession -> setPlayer( null ) ;

        $this -> gameSessions -> removeElement( $gameSession ) ;
    }


    /**
     * Get gameSessions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGameSessions()
    {
        return $this -> gameSessions ;
    }


    public function getGameSessionsAreEmpty()
    {

        return ( 0 == count ( $this -> gameSessions ) ) ? true : false ;

    }




    /**
     * Add table
     *
     * @param \Game\Poker\CoreBundle\Entity\PokerTable $table
     *
     * @return PokerUser
     */
    public function addTable( PokerTable $table )
    {
        $table -> addPlayer( $this ) ;

        $this -> tables[] = $table ;

        return $this ;
    }


    /**
     * Remove table
     *
     * @param \Game\Poker\CoreBundle\Entity\PokerTable $table
     */
    public function removeTable( PokerTable $table )
    {
        $table -> removePlayer( $this ) ;

        $this -> tables -> removeElement( $table ) ;
    }


    /**
     * Get tables
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTables()
    {
        return $this -> tables ;
    }

    public function getLastAddedTable()
    {
        return $this -> tables[ count( $this -> tables ) - 1 ] ;
    }


    public function getCurrentTableSession( PokerTable $table )
    {
        if( ! $this -> getGameSessionsAreEmpty()  )
        {

            foreach( $this -> getGameSessions() as $gameSession )
            {

                if ( $gameSession -> getTable() -> getId() == $table -> getId() )
                {

                    return $gameSession ;

                }
                else
                {

                    continue ;

                }

            }

        }


        $this -> addTable( $table ) ;

        return new GameSession() ;


    }


    public function sitDownAt( PokerTable $table )
    {

        $gameSession = $this -> getCurrentTableSession( $table ) ;

        if( $gameSession -> isNew() )
        {
            $buyIn = $table -> getMaxBuyIn() ;

            $this -> makeCashWithDrawOf( $buyIn , $gameSession ) ;

            $this -> addGameSession( $gameSession ) ;
        }

        return $gameSession ;

    }


}
