<?php
/**
 * Created by PhpStorm.
 * User: jo
 * Date: 04/02/17
 * Time: 14:55
 */

namespace Game\Poker\CoreBundle\Traits;


trait CommonPokerActions
{

    protected $em              ;
    protected $game            ;
    protected $table           ;
    protected $round           ;
    protected $dealer          ;
    protected $deck            ;

    protected $player          ;
    protected $gameSession     ;
    protected $sessionMessages ;


    public function playersBelongsToTable(array $parameters)
    {
        return $this -> game -> getTable( $parameters[ 'tableId' ] ) -> getPlayer( $parameters[ 'playerId' ] ) -> isEmpty() ;
    }

    public function requestedPlayerIsCurrentPlayer(array $parameters)
    {
        return $parameters[ 'playerId' ] == $this -> player -> getId() ;
    }

    public function gameIsNotStarted(array $parameters = null)
    {
        return ( $this -> round ->getName() == 'Init' AND ! $this -> round -> getIsReady() ) ;
    }

    public function hasEnoughPlayers(array $parameters = null)
    {
        return 1 < count($this -> table -> getPlayers()) ;
    }

}