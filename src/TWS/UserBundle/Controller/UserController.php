<?php

namespace TWS\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use TWS\UserBundle\Form\ImageType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class UserController extends Controller {

    public function userAction(Request $request) {
        $user = $this->getUser();

        $form = $this->createFormBuilder($user)
                ->add('image', new ImageType(), array('required' => false) )
                ->add('save', SubmitType::class)
                ->getForm();

        if ($form->handleRequest($request)->isValid()) {            
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
        }
        return $this->render('TWSUserBundle::profil.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

}
