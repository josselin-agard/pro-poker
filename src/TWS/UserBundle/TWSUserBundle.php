<?php

namespace TWS\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TWSUserBundle extends Bundle
{
    public function getParent() {
        return 'FOSUserBundle';
    }
}
