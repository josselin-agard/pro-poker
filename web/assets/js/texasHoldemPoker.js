/* fonction qui permet de déterminer presque automatiquement l'url de base à rajouter avant le nom de la route pour faire les requêtes ajax */

var game    ;
var table   ;
var round   ;
var players ;
var player  ;
var messages ;

var nbPlayers = 0 ;

var globalMatch     ;
var controllerMatch ;


function getRequestUrl()
{
    var requestUrl;
    var directoryIndex = ' '; //Si ça ne fonctionne pas et que tu as app_dev.php ou index.php dans l'URL, change cette variable par ça.
    reg = new RegExp( directoryIndex , 'g' ) ;

    var hasDirectoryIndex = window . location . pathname . match( reg ) ;

    var protocol = window . location.  protocol + '//' ;

    var hostname = window . location . hostname ;

    var pathname = window . location . pathname . split( reg )[ 0 ] ;

    if ( ! hasDirectoryIndex )

        requestUrl = protocol + hostname + pathname ;

    else

        requestUrl = protocol + hostname + pathname + directoryIndex ;

    return requestUrl ;
}

function getImagePath()
{
    var imagePath ;
    var endPath = new RegExp( '(/poker/)(([a-zA-Z-]+/?){2})(/game/[0-9]+/table/[0-9]+)$' , 'g' ) ;

    imagePath = window . location . pathname . replace( endPath , '/assets/images/' ) ;

    return  imagePath ;

}


function setCards( imageSelector , response )
{
    var suit ;
    var rank ;

    for ( i in response )
    {
        suit = null ;
        rank = null ;

        for ( j in response[ i ] )
        {

            if ( response[ i ][ j ] <= 10 )
            {
                rank = response[ i ][ j ] ;

            }
            else
            {

                // permet de mettre systématiquement en minuscule pour être sûr qu'il y aura pas de problèmes, parce que pour le switch/case 'Coeur' et 'coeur' c'est pas la même chose.
                switch ( response[ i ][ j ] . toLowerCase() )
                {
                    case 'valet' :
                        rank = 'j' ;
                        break ;

                    case 'dame' :
                        rank = 'q' ;
                        break ;

                    case 'roi' :
                        rank = 'k' ;
                        break ;

                    case 'as' :
                        rank = 'a' ;
                        break ;

                    case 'coeur' :
                        suit = 'h' ;
                        break ;

                    case 'carreau' :
                        suit = 'd' ;
                        break ;

                    case 'pique' :
                        suit = 's' ;
                        break ;

                    case 'trefle' :
                        suit = 'c' ;
                        break ;
                }
            }

        }

        /* On défini une expression régulière avec le slash, séparateur utilisé entre les noms de dossier dans les chemins de fichier */
        var separator = '/' ;
        var regex = new RegExp( separator , "g" ) ;

        /* on récupère le chemin contenu dans l'attribut src de l'image et on utilise la fonction javascript "split" (séparer) en lui donnant l'expression régulière en paramètre
         pour qu'elle sépare la chaîne de caractère de src à chaque fois qu'elle voit un slash dedans. Elle retourne un tableau contenant chaque case une partie du chemin initial,
         sans les slashs de séparation.
         */

        var cardNumber = parseInt( i ) + 1 ;

        var image = $( imageSelector + cardNumber ) ;

        console.log(i);

        console.log( image ) ;

        if( 0 != image.length )
        {
            var split_path = image . attr( "src" ) . split( regex ) ;

            /* Du coup on sait que dans la dernière case du tableau, il y a la dernière partie du chemin, c'est-à-dire, "b.gif" pour l'instant.
             Donc on sélectionne cette dernière partie et on l'écrase avec notre nouveau nom d'image.
             */
            split_path[ split_path . length - 1 ] = rank + suit + ".gif" ;

            /* On recolle chaque partie du tableau en une seule chaîne avec des slashs grâce à la fonction "join" */
            var image_path = split_path . join( separator ) ;

            image . attr( "src" , image_path ) ;
            image . addClass( 'dealt' ) ;
            image . show() ;
        }

    }
}


function refreshIfNewPlayer( newPlayerNb )
{
    if( nbPlayers == 0 )
    {
        nbPlayers = newPlayerNb ;

        return false
    }

    else if( nbPlayers !=  newPlayerNb )
    {
        return true ;
    }

    return false ;
}


function printMessage( message )
{
    $( 'div.tchat section.messages' ) . prepend('<p>' + message + '</p>') ;

}


function getAction( url , match , message )
{
    var methodReg = new RegExp( match , 'g' ) ;
    var requestUrl = getRequestUrl() . replace( methodReg , '' ) + '/' + url ;

    printMessage( message ) ;

    $.ajax({
        url:  requestUrl ,
        type: 'get' ,
        dataType: 'json',
        success: function ( response )
        {
            if( response[ 'status' ] == 'done' )
            {

                for( message in response[ 'messages' ] )

                    if( response[ 'messages' ][message] != '' )

                        printMessage( "<span class=\"success\">" + response[ 'messages' ][message] + "</span>" ) ;



                if( 0 != response[ 'nextAction' ] . length )
                {

                    var match ;

                    switch( response[ 'nextAction' ][ 'match' ] )
                    {
                        case 'globalMatch' :
                            match = globalMatch ;
                            break ;

                        case 'controllerMatch' :
                            match = controllerMatch ;
                            break ;
                    }


                    getAction( response[ 'nextAction' ][ 'url' ] , match , response[ 'nextAction' ][ 'message' ] ) ;

                }

            }
            else

                printMessage( "<span class=\"error\">Une erreur s'est produite !</span>" ) ;
        }
    }) ;
}


function getCards()
{
    var methodReg = new RegExp( controllerMatch + '((/[0-9]+){2})$' , 'g' ) ;
    var requestUrl = getRequestUrl() . replace( methodReg , '' ) + '/getCards' ;

    $.ajax({
        url: requestUrl,
        type: 'get',
        dataType: 'json',
        success: function ( response )
        {
            setCards( 'div#player-' + $('div.table').data("current-player") + ' img.card' , response ) ;
        }
    });
}

function whoIsInplay()
{
    for( tableplayer in players[ 'playersInPlay' ] )
    {
        if( players[ 'playersInPlay' ][ tableplayer ] )

            $( 'div#' + tableplayer + ' img[class^="card"]' ).show() ;
    }
}

function setBlindsPosition()
{
    if( 0 == $( 'img.smallblind' ) .length )
    {
        $( 'div#player-' + round[ 'smallBlind' ] ) . append( '<img class="smallblind" src="' + getImagePath() + 'smallblind.png" />') ;
    }

    if( 0 == $( 'img.bigblind' ) .length )
    {
        $( 'div#player-' + round[ 'bigBlind' ] ) . append( '<img class="bigblind" src="' + getImagePath() + 'bigblind.png" />') ;
    }
}


function isLastBet()
{
    if( round[ 'isPlayTime' ] )
    {
        if( null != round[ 'lastBet' ] && 0 != round[ 'lastBet' ] && ! round[ 'betsAreEqual' ] )
        {
            $( '.passer' ) . hide() ;

            $('.coucher') . show() ;
            $('.suivre') . show() ;
        }
        else if( null == round[ 'lastBet' ] || ( null != round[ 'lastBet' ] && round[ 'betsAreEqual' ] ) )
        {
            $('.coucher') . hide() ;
            $('.suivre') . hide() ;

            $( '.passer' ) . show() ;
        }
        else
        {
            $('.passer') . show() ;
            $('.coucher') . show() ;
            $('.suivre') . show() ;
        }
    }
}


function amICurrentPlayer()
{
    var playerIdReg = new RegExp( '-' , 'g' ) ;

    if ( 0 != $( 'div.currentplayer' ) . length )
    {

        if( player[ 'myId' ] == $( 'div.currentplayer' ).attr( 'id' ) . split( playerIdReg )[ 1 ] && round[ 'isPlayTime' ] )
        {

            $( 'div.actions' ) . show() ;
            isLastBet() ;
        }
        else

            $( 'div.actions' ) . hide() ;
    }

}


function highLightCurrentPlayer()
{
    var currentPlayerId = null ;
    var playerIdReg = new RegExp( '-' , 'g' ) ;

    if ( 0 != $( 'div.currentplayer' ) . length ) currentPlayerId = $( 'div.currentplayer' ).attr( 'id' ) . split( playerIdReg )[ 1 ] ;

    if( currentPlayerId != players[ 'currentPlayer' ] && round[ 'isPlayTime' ] || ( currentPlayerId != players[ 'currentPlayer' ] && round[ 'isReady' ] && round[ 'isWaiting' ] ) )
    {
        $( 'div.currentplayer' ) . removeClass( 'currentplayer' ) ;
        $( 'div#player-' + players[ 'currentPlayer' ] ) . addClass(  'currentplayer'  ) ;

    }

    amICurrentPlayer() ;
}


function updateBuyIns()
{
    for( tableplayer in players[ 'playersChips' ] )
    {
        $( 'div#' + tableplayer + ' p.chips' ) . html( players[ 'playersChips' ][ tableplayer ] + '$' ) ;
    }
}


function setCurrentBet()
{
    for( tableplayer in players[ 'playersCurrentBet' ] )
    {
        if( round[ 'isGameStarted'] && 0 == players[ 'playersCurrentBet' ][ tableplayer ] )
        {
            $( 'div#' + tableplayer + ' span.currentBet > span.amount' ).html( '0$' ) ;
            $( 'div#' + tableplayer + ' span.currentBet' ) . hide() ;
        }
        else if( 0 == $( 'div#' + tableplayer + ' span.currentBet' ) . length &&  null != players[ 'playersCurrentBet' ][ tableplayer ] )
        {
            var bet = $( '<span class="currentBet"> <img class="money" src="' + getImagePath() + 'money.png" /> <span class="amount">' + players[ 'playersCurrentBet' ][ tableplayer ] + '$</span> </span>' ) ;

            $( 'div#' + tableplayer ) . append( bet ) ;

        }
        else
        {
            $( 'div#' + tableplayer + ' span.currentBet' ) . show() ;

            $( 'div#' + tableplayer + ' span.currentBet > span.amount' ).html( players[ 'playersCurrentBet' ][ tableplayer ]+'$' ) ;

        }
    }
}

function setTablePot()
{

    if( 0 == $( 'span.tablePot' ) . length &&  null != table[ 'pot' ] )
    {
        var pot = $( '<span class="tablePot"> <img class="money" src="' + getImagePath() + 'money.png" /> <img  style="position: relative; right: 1.8em;" class="money" src="' + getImagePath() + 'money.png" /> <span class="amount">' + table[ 'pot' ] + '$</span> </span>' ) ;

        $( 'div.table' ) . append( pot ) ;

    }
    else if( $( 'span.tablePot' ) . html() != table[ 'pot' ] + '$' )

        $( 'span.tablePot span.amount' ).html( table[ 'pot' ] + '$' ) ;

}

function setBoardCards()
{
    if( 0 != table[ 'board' ][ 'flop' ] . length && 0 == $( 'div.table table#board td.flop img.dealt' ) . length )
        setCards( 'div.table table#board td.flop img.card' , table[ 'board' ][ 'flop' ] ) ;


    if( 0 != table[ 'board' ][ 'turn' ] . length && 0 == $( 'div.table table#board td.turn img.dealt' ) . length )
        setCards( 'div.table table#board td.turn img.card' , table[ 'board' ][ 'turn' ] ) ;


    if( 0 != table[ 'board' ][ 'river' ] . length && 0 == $( 'div.table table#board td.river img.dealt' ) . length )
        setCards( 'div.table table#board td.river img.card' , table[ 'board' ][ 'river' ] ) ;
}


function refreshTableInfos()
{

    if( 0 == $('div#player-' + $('div.table').data("current-player") + ' img.dealt') . length && 0 != player[ 'myHand' ] . length )
    {
        setCards( 'div#player-' + $('div.table').data("current-player") + ' img.card' , player[ 'myHand' ] ) ;
    }


    whoIsInplay() ;


    setBlindsPosition() ;


    highLightCurrentPlayer() ;


    updateBuyIns() ;


    setCurrentBet() ;


    setTablePot() ;


    setBoardCards() ;

}


function fold()
{
    var methodReg = new RegExp( controllerMatch , 'g' ) ;
    var requestUrl = getRequestUrl() . replace( methodReg , '' ) + '/fold' ;

    printMessage( $( 'div#player-' + players[ 'currentPlayer' ] ) . data( 'pseudo' ) + ' se couche...' ) ;

    /*$.ajax({
        url:  requestUrl ,
        type: 'get' ,
        dataType: 'json',
        success: function ( response )
        {

        }
    }) ;*/
}


function check()
{
    var methodReg = new RegExp( controllerMatch , 'g' ) ;
    var requestUrl = getRequestUrl() . replace( methodReg , '' ) + '/check' ;

    printMessage(  $( 'div#player-' + players[ 'currentPlayer' ] ) . data( 'pseudo' ) + ' passe ...' ) ;

    $.ajax({
     url:  requestUrl ,
     type: 'get' ,
     dataType: 'json',
         success: function ( response )
         {

         }
     }) ;
}


function call()
{
    var methodReg = new RegExp( controllerMatch , 'g' ) ;
    var requestUrl = getRequestUrl() . replace( methodReg , '' ) + '/call' ;

    printMessage(  $( 'div#player-' + players[ 'currentPlayer' ] ) . data( 'pseudo' ) + ' suit...' ) ;

    $.ajax({
        url:  requestUrl ,
        type: 'get' ,
        dataType: 'json',
        success: function ( response )
        {

        }
    }) ;

}


function bet()
{

    var amount = parseFloat( $( 'input[name="amount"]' ).val() ) ;

    if( undefined != amount && 0 < amount )
    {

        printMessage(  $( 'div#player-' + players[ 'currentPlayer' ] ) . data( 'pseudo' ) + ' mise de : ' + amount + '$' ) ;

        var methodReg = new RegExp( controllerMatch , 'g' ) ;
        var requestUrl = getRequestUrl() . replace( methodReg , '' ) + '/bet-amount/' + amount ;

        $.ajax({
         url:  requestUrl ,
         type: 'get' ,
         dataType: 'json',
             success: function ( response )
             {

             }
         }) ;

    }
    else
    {
        printMessage( 'Vous devez saisir un montant correcte !' ) ;
    }

}


function refreshGame()
{
    var refreshReg = new RegExp( "(/poker/)(([a-zA-Z-]+/?){2})(/game/[0-9]+/table/[0-9]+)$" , "g" ) ;
    var requestUrl = getRequestUrl() . replace( refreshReg , "$1refresh$4" ) ;

    $.ajax({
        url:  requestUrl ,
        type: 'get' ,
        dataType: 'json',
        success: function ( response )
        {
            game    = response[ 'game' ]   ;
            table   = game[ 'table' ]      ;
            round   = table[ 'round' ]     ;
            messages = table[ 'message' ]   ;

            players = game[ 'players' ]    ;
            player  = response[ 'player' ] ;

            globalMatch = '(' + game[ 'variant' ] + '/' + game[ 'mode' ] + '/)' ;
            controllerMatch = '(' + game[ 'mode' ] + '/)' ;


            if( messages != undefined )
            {

                for( message in messages )

                    if( messages[message] != '' )

                        printMessage("<span class=\"info\">" +messages[message] + "</span>");

            }

            if( refreshIfNewPlayer( game[ 'players' ][ 'playersNb' ] ) )
            {
                window.location.reload() ;
            }
            else
            {

                refreshTableInfos();

                if( 1 < game[ 'players' ][ 'playersNb' ] )
                {

                    switch( round[ 'current' ] )
                    {
                        case 'INIT' :

                            if( ! round[ 'isGameStarted' ] && game[ 'players' ][ 'allowedPlayer' ] == $('div.table').data("current-player") )
                            {

                                printMessage( 'Nous sommes maintenant assez nombreux.' ) ;
                                getAction( "start" , globalMatch , "Initialisation..." ) ;

                            }

                            else if( round[ 'isPlayTime' ] && 0 == player[ 'myHand' ] .length )
                            {

                                printMessage( 'Distribution de vos cartes...' ) ;
                                getCards() ;

                            }

                            else if( round[ 'isComplete' ] && round[ 'isPlayTime' ] )
                            {
                                printMessage( 'C\'est à ' + $( 'div#player-' + players[ 'currentPlayer' ] ) . data( 'pseudo' ) + ' de jouer.' ) ;
                                printMessage( 'en attente...' ) ;

                            }

                            else
                            {
                                printMessage( 'en attente...' ) ;
                            }

                            break;


                        case 'FLOP' :
                        case 'TURN' :
                        case 'RIVER' :

                            if( round[ 'isReady' ] && round[ 'isWaiting' ] && game[ 'players' ][ 'allowedPlayer' ] == $('div.table').data("current-player") )
                            {
                                getAction( "get-" + round[ 'current' ] . toLowerCase() , controllerMatch , "Préparation du " + round[ 'current' ] + " en cours." ) ;

                            }
                            else if( round[ 'isComplete' ] && round[ 'isPlayTime' ] )
                            {
                                printMessage( 'C\'est à ' + $( 'div#player-' + players[ 'currentPlayer' ] ) . data( 'pseudo' ) + ' de jouer.' ) ;
                                printMessage( 'en attente...' ) ;

                            }
                            else
                            {
                                printMessage( 'en attente...' ) ;
                            }

                            break;


                        case 'SHOWDOWN' :

                            break;

                    }

                }
                else
                {
                    printMessage( 'Il faut attendre au moins au autre joueur pour commencer la partie.' ) ;
                }

            }
        } ,

        complete: function ()
        {
            // Schedule the next request when the current one's complete

            setTimeout( refreshGame , 500 ) ;
        }

    }) ;

}



/********************************************************************** LANCEMENT DU JEU *****************************************************************/

$( document ) . ready( function ()
{

    $( 'div.actions .coucher' ).off().on( 'click' , function()
    {

        fold() ;

    }) ;

    $( 'div.actions .passer' ).off().on( 'click' , function()
    {

        check() ;

    }) ;

    $( 'div.actions .suivre' ).off().on( 'click' , function()
    {

        call() ;

    }) ;

    $( 'div.actions .miser' ).off().on( 'click' , function()
    {

        bet() ;

    }) ;

    refreshGame() ;

}) ;
